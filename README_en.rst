==============================================
visint incremental learning package
==============================================


.. image:: https://img.shields.io/pypi/v/visintincremental.svg
        :target: https://pypi.python.org/pypi/visintincremental

.. image:: https://readthedocs.org/projects/visintincremental/badge/?version=latest
        :target: https://visintincremental.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

Incremental learning package, including commonly used datasets, classic IL models, and some tools.

* MIT license
* Doc: https://visintincremental.readthedocs.io.
* Code: https://gitee.com/swj123/visintincremental.git

Get Started!
------------

Ready to contribute? Here's how to set up `visintincremental` for local development.

1. install `visintincremental` with pip::

    $ pip install visintincremental

2. import `visintincremental` in  .py files::

    $ import visintincremental
    $ from visintincremental.utils.args import get_args
    $ from visintincremental.utils.training import train_il

3. inital args::

    $ args = get_args()
    $
    $ args.model = 'lwf'              # model name
    $ args.dataset = 'seq-mnist'      # dataset name
    $ args.print_freq = 1
    $ args.n_epochs = 1
    $ args.device = 'cpu'
    $
    $ args.lr = 0.02                  # learning rate
    $ args.batch_size = 64            # batch size
    $ args.alpha = 5
    $ args.softmax_temp = 2.0
    $ args.wd_reg = 5e-5

   Now you can make your changes locally.

4. incremental learning::

    $ train_il(args)

5. wait for training.






