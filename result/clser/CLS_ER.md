# CLS_ER

> Title: Gradient Episodic Memory for Continual Learning
>
> Conference: ICLR2022
>
> URL: https://openreview.net/forum?id=uxxFrDwrE7Y

```
@inproceedings{
    arani2022learning,
    title={Learning Fast, Learning Slow: A General Continual Learning Method based on Complementary Learning System},
    author={Elahe Arani and Fahad Sarfraz and Bahram Zonooz},
    booktitle={International Conference on Learning Representations},
    year={2022},
    url={https://openreview.net/forum?id=uxxFrDwrE7Y}
}
```

### CIFAR10

### buffer_size = 200

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.n_epochs = 50
args.buffer_size = 200

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 0.15
args.stable_model_update_freq = 0.1
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.3
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.45 % 	 [Task-IL]: 98.45 %
Accuracy for 2 task(s): 	 [Class-IL]: 78.35 % 	 [Task-IL]: 95.18 %
Accuracy for 3 task(s): 	 [Class-IL]: 58.15 % 	 [Task-IL]: 92.72 %
Accuracy for 4 task(s): 	 [Class-IL]: 52.59 % 	 [Task-IL]: 92.9 %
Accuracy for 5 task(s): 	 [Class-IL]: 48.23 % 	 [Task-IL]: 92.87 %
Accuracy for 5 task(s): 	 [Class-IL]: 47.21 % 	 [Task-IL]: 92.04 %
Accuracy for 5 task(s): 	 [Class-IL]: 50.23 % 	 [Task-IL]: 90.26 %
Accuracy for 5 task(s): 	 [Class-IL]: 47.59 % 	 [Task-IL]: 91.57 %
Accuracy for 5 task(s): 	 [Class-IL]: 49.71 % 	 [Task-IL]: 92.98 %

>   Average: 48.594/91.944

### buffer_size = 500

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.n_epochs = 50
args.buffer_size = 500

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 0.15
args.stable_model_update_freq = 0.1
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.9
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.3 % 	 [Task-IL]: 98.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 84.9 % 	 [Task-IL]: 94.8 %
Accuracy for 3 task(s): 	 [Class-IL]: 68.62 % 	 [Task-IL]: 93.42 %
Accuracy for 4 task(s): 	 [Class-IL]: 62.68 % 	 [Task-IL]: 93.66 %
Accuracy for 5 task(s): 	 [Class-IL]: 64.58 % 	 [Task-IL]: 93.15 %
Accuracy for 5 task(s): 	 [Class-IL]: 63.65 % 	 [Task-IL]: 93.91 %
Accuracy for 5 task(s): 	 [Class-IL]: 65.82 % 	 [Task-IL]: 93.84 %
Accuracy for 5 task(s): 	 [Class-IL]: 63.74 % 	 [Task-IL]: 93.41 %
Accuracy for 5 task(s): 	 [Class-IL]: 62.74 % 	 [Task-IL]: 93.36 %

>   Average: 64.106 / 93.534

### buffer_size = 5120

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.n_epochs = 50
args.buffer_size = 5120

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 0.15
args.stable_model_update_freq = 0.8
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 1.0
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.65 % 	 [Task-IL]: 98.65 %
Accuracy for 2 task(s): 	 [Class-IL]: 92.4 % 	 [Task-IL]: 96.25 %
Accuracy for 3 task(s): 	 [Class-IL]: 85.02 % 	 [Task-IL]: 96.5 %
Accuracy for 4 task(s): 	 [Class-IL]: 85.42 % 	 [Task-IL]: 97.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.99 % 	 [Task-IL]: 96.94 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.39 % 	 [Task-IL]: 96.8 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.07 % 	 [Task-IL]: 97.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.61 % 	 [Task-IL]: 96.77 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.72 % 	 [Task-IL]: 97.14 %

>   Average: 84.956 / 96.93

### MNIST

### buffer_size = 200

```
# args.dataset = 'seq-mnist'
# args.print_freq = 10
# args.n_epochs = 10
# args.buffer_size = 200
#
# args.batch_size = 32
# args.lr = 0.03
# args.minibatch_size = 32
# args.reg_weight = 2.0
# args.stable_model_update_freq = 0.9
# args.stable_model_alpha = 0.99
# args.plastic_model_update_freq = 1.0
# args.plastic_model_alpha = 0.99
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.86 % 	 [Task-IL]: 99.86 %
Accuracy for 2 task(s): 	 [Class-IL]: 97.16 % 	 [Task-IL]: 99.21 %
Accuracy for 3 task(s): 	 [Class-IL]: 93.43 % 	 [Task-IL]: 99.32 %
Accuracy for 4 task(s): 	 [Class-IL]: 89.81 % 	 [Task-IL]: 99.04 %
Accuracy for 5 task(s): 	 [Class-IL]: 80.81 % 	 [Task-IL]: 98.8 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.60 % 	 [Task-IL]: 98.86 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.15 % 	 [Task-IL]: 98.91 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.07 % 	 [Task-IL]: 98.82 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.07 % 	 [Task-IL]: 98.82 %

> Average: 83.34/98.842

### buffer_size = 500

```
# args.dataset = 'seq-mnist'
# args.print_freq = 10
# args.n_epochs = 10
# args.buffer_size = 200
#
# args.batch_size = 32
# args.lr = 0.1
# args.minibatch_size = 32
# args.reg_weight = 2.0
# args.stable_model_update_freq = 0.9
# args.stable_model_alpha = 0.99
# args.plastic_model_update_freq = 1.0
# args.plastic_model_alpha = 0.99
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.22 % 	 [Task-IL]: 99.64 %
Accuracy for 3 task(s): 	 [Class-IL]: 93.32 % 	 [Task-IL]: 99.55 %
Accuracy for 4 task(s): 	 [Class-IL]: 90.71 % 	 [Task-IL]: 99.2 %
Accuracy for 5 task(s): 	 [Class-IL]: 82.28 % 	 [Task-IL]: 98.9 %
Accuracy for 5 task(s): 	 [Class-IL]: 82.44 % 	 [Task-IL]: 98.2 %
Accuracy for 5 task(s): 	 [Class-IL]: 81.31 % 	 [Task-IL]: 97.93 %
Accuracy for 5 task(s): 	 [Class-IL]: 82.95 % 	 [Task-IL]: 99.03 %
Accuracy for 5 task(s): 	 [Class-IL]: 82.05 % 	 [Task-IL]: 98.57 %

> Average: 82.206/98.526

### buffer_size = 5120

```
args.dataset = 'seq-mnist'
args.print_freq = 10
args.n_epochs = 10
args.buffer_size = 200

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 2.0
args.stable_model_update_freq = 0.8
args.stable_model_alpha = 0.99
args.plastic_model_update_freq = 1.0
args.plastic_model_alpha = 0.99
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.17 % 	 [Task-IL]: 99.66 %
Accuracy for 3 task(s): 	 [Class-IL]: 94.3 % 	 [Task-IL]: 99.34 %
Accuracy for 4 task(s): 	 [Class-IL]: 90.92 % 	 [Task-IL]: 99.23 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.28 % 	 [Task-IL]: 99.11 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.69 % 	 [Task-IL]: 98.85 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.57 % 	 [Task-IL]: 98.53 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.63 % 	 [Task-IL]: 98.78 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.21 % 	 [Task-IL]: 98.96 %

> Average: 83.876/98.846

### CIFAR100

### buffer_size = 200

```
args.dataset = 'seq-cifar100'
args.print_freq = 10
args.n_epochs = 100
args.buffer_size = 200

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 0.15
args.stable_model_update_freq = 0.6
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.7
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 85.9 % 	 [Task-IL]: 85.9 %
Accuracy for 2 task(s): 	 [Class-IL]: 58.65 % 	 [Task-IL]: 79.3 %
Accuracy for 3 task(s): 	 [Class-IL]: 46.9 % 	 [Task-IL]: 77.17 %
Accuracy for 4 task(s): 	 [Class-IL]: 37.43 % 	 [Task-IL]: 72.65 %
Accuracy for 5 task(s): 	 [Class-IL]: 30.2 % 	 [Task-IL]: 70.4 %
Accuracy for 6 task(s): 	 [Class-IL]: 28.08 % 	 [Task-IL]: 70.23 %
Accuracy for 7 task(s): 	 [Class-IL]: 25.37 % 	 [Task-IL]: 66.43 %
Accuracy for 8 task(s): 	 [Class-IL]: 19.51 % 	 [Task-IL]: 65.95 %
Accuracy for 9 task(s): 	 [Class-IL]: 17.71 % 	 [Task-IL]: 62.33 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.1 % 	 [Task-IL]: 61.49 %
Accuracy for 10 task(s): 	 [Class-IL]: 14.78 % 	 [Task-IL]: 58.25 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.99 % 	 [Task-IL]: 63.55 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.2 % 	 [Task-IL]: 60.65 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.34 % 	 [Task-IL]: 62.62 %

>   Average: 15.282/61.312

### buffer_size = 500

```
args.dataset = 'seq-cifar100'
args.print_freq = 10
args.n_epochs = 100
args.buffer_size = 500

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 0.15
args.stable_model_update_freq = 0.6
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.9
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 88.1 % 	 [Task-IL]: 88.1 %
Accuracy for 2 task(s): 	 [Class-IL]: 68.3 % 	 [Task-IL]: 83.6 %
Accuracy for 3 task(s): 	 [Class-IL]: 58.33 % 	 [Task-IL]: 82.07 %
Accuracy for 4 task(s): 	 [Class-IL]: 50.88 % 	 [Task-IL]: 79.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 42.72 % 	 [Task-IL]: 79.28 %
Accuracy for 6 task(s): 	 [Class-IL]: 40.55 % 	 [Task-IL]: 77.53 %
Accuracy for 7 task(s): 	 [Class-IL]: 37.37 % 	 [Task-IL]: 75.44 %
Accuracy for 8 task(s): 	 [Class-IL]: 32.0 % 	 [Task-IL]: 74.06 %
Accuracy for 9 task(s): 	 [Class-IL]: 29.6 % 	 [Task-IL]: 72.36 %
Accuracy for 10 task(s): 	 [Class-IL]: 25.66 % 	 [Task-IL]: 71.28 %
Accuracy for 10 task(s): 	 [Class-IL]: 23.86 % 	 [Task-IL]: 71.91 %
Accuracy for 10 task(s): 	 [Class-IL]: 24.31 % 	 [Task-IL]: 72.1 %
Accuracy for 10 task(s): 	 [Class-IL]: 25.39 % 	 [Task-IL]: 72.38 %
Accuracy for 10 task(s): 	 [Class-IL]: 24.49 % 	 [Task-IL]: 72.9 %

>   Average: 24.742 / 72.114

### buffer_size = 5120

```
args.dataset = 'seq-cifar100'
args.print_freq = 10
args.n_epochs = 100
args.buffer_size = 5120

args.batch_size = 32
args.lr = 0.1
args.minibatch_size = 32
args.reg_weight = 0.15
args.stable_model_update_freq = 0.6
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.8
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 89.5 % 	 [Task-IL]: 89.5 %
Accuracy for 2 task(s): 	 [Class-IL]: 81.45 % 	 [Task-IL]: 89.2 %
Accuracy for 3 task(s): 	 [Class-IL]: 76.73 % 	 [Task-IL]: 89.77 %
Accuracy for 4 task(s): 	 [Class-IL]: 72.32 % 	 [Task-IL]: 88.5 %
Accuracy for 5 task(s): 	 [Class-IL]: 67.84 % 	 [Task-IL]: 88.12 %
Accuracy for 6 task(s): 	 [Class-IL]: 67.18 % 	 [Task-IL]: 88.37 %
Accuracy for 7 task(s): 	 [Class-IL]: 64.51 % 	 [Task-IL]: 87.17 %
Accuracy for 8 task(s): 	 [Class-IL]: 60.72 % 	 [Task-IL]: 87.55 %
Accuracy for 9 task(s): 	 [Class-IL]: 60.12 % 	 [Task-IL]: 87.37 %
Accuracy for 10 task(s): 	 [Class-IL]: 55.91 % 	 [Task-IL]: 87.3 %
Accuracy for 10 task(s): 	 [Class-IL]: 56.15 % 	 [Task-IL]: 87.63 %
Accuracy for 10 task(s): 	 [Class-IL]: 56.39 % 	 [Task-IL]: 87.43 %
Accuracy for 10 task(s): 	 [Class-IL]: 55.43 % 	 [Task-IL]: 87.04 %
Accuracy for 10 task(s): 	 [Class-IL]: 55.25 % 	 [Task-IL]: 87.4 %

>   Average: 55.826/87.36

### Tiny-ImageNet

### buffer_size = 200

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100
args.buffer_size = 200

args.lr = 0.05
args.batch_size = 32
args.minibatch_size = 32
args.reg_weight = 0.1
args.stable_model_update_freq = 0.04
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.08
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 78.1 % 	 [Task-IL]: 78.1 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.3 % 	 [Task-IL]: 69.1 %
Accuracy for 3 task(s): 	 [Class-IL]: 35.1 % 	 [Task-IL]: 65.73 %
Accuracy for 4 task(s): 	 [Class-IL]: 28.28 % 	 [Task-IL]: 61.92 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.44 % 	 [Task-IL]: 59.58 %
Accuracy for 6 task(s): 	 [Class-IL]: 16.37 % 	 [Task-IL]: 56.6 %
Accuracy for 7 task(s): 	 [Class-IL]: 13.74 % 	 [Task-IL]: 53.3 %
Accuracy for 8 task(s): 	 [Class-IL]: 12.14 % 	 [Task-IL]: 51.1 %
Accuracy for 9 task(s): 	 [Class-IL]: 9.9 % 	 [Task-IL]: 48.37 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.74 % 	 [Task-IL]: 46.03 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.12 % 	 [Task-IL]: 46.32 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.67 % 	 [Task-IL]: 45.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.64 % 	 [Task-IL]: 45.28 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.09 % 	 [Task-IL]: 46.49 %

>   Average: 9.852/46.002

### buffer_size = 500

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100
args.buffer_size = 500

args.batch_size = 32
args.lr = 0.05
args.minibatch_size = 32
args.reg_weight = 0.1
args.stable_model_update_freq = 0.05
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.08
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 77.0 % 	 [Task-IL]: 77.0 %
Accuracy for 2 task(s): 	 [Class-IL]: 54.1 % 	 [Task-IL]: 70.5 %
Accuracy for 3 task(s): 	 [Class-IL]: 43.2 % 	 [Task-IL]: 68.9 %
Accuracy for 4 task(s): 	 [Class-IL]: 38.9 % 	 [Task-IL]: 68.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 32.08 % 	 [Task-IL]: 66.96 %
Accuracy for 6 task(s): 	 [Class-IL]: 23.6 % 	 [Task-IL]: 65.03 %
Accuracy for 7 task(s): 	 [Class-IL]: 21.19 % 	 [Task-IL]: 63.09 %
Accuracy for 8 task(s): 	 [Class-IL]: 18.71 % 	 [Task-IL]: 60.98 %
Accuracy for 9 task(s): 	 [Class-IL]: 16.24 % 	 [Task-IL]: 59.53 %
Accuracy for 10 task(s): 	 [Class-IL]: 14.4 % 	 [Task-IL]: 56.77 %
Accuracy for 10 task(s): 	 [Class-IL]: 13.91 % 	 [Task-IL]: 56.99 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.02 % 	 [Task-IL]: 58.32 %
Accuracy for 10 task(s): 	 [Class-IL]: 16.13 % 	 [Task-IL]: 57.57 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.07 % 	 [Task-IL]: 56.53 %

>   Average: 14.906 / 57.236

### buffer_size = 5120

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100
args.buffer_size = 5120

args.batch_size = 32
args.lr = 0.05
args.minibatch_size = 32
args.reg_weight = 0.1
args.stable_model_update_freq = 0.07
args.stable_model_alpha = 0.999
args.plastic_model_update_freq = 0.08
args.plastic_model_alpha = 0.999
```

Accuracy for 1 task(s): 	 [Class-IL]: 77.7 % 	 [Task-IL]: 77.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 68.55 % 	 [Task-IL]: 76.7 %
Accuracy for 3 task(s): 	 [Class-IL]: 63.27 % 	 [Task-IL]: 76.5 %
Accuracy for 4 task(s): 	 [Class-IL]: 60.75 % 	 [Task-IL]: 76.8 %
Accuracy for 5 task(s): 	 [Class-IL]: 56.7 % 	 [Task-IL]: 77.2 %
Accuracy for 6 task(s): 	 [Class-IL]: 51.2 % 	 [Task-IL]: 76.27 %
Accuracy for 7 task(s): 	 [Class-IL]: 49.74 % 	 [Task-IL]: 75.99 %
Accuracy for 8 task(s): 	 [Class-IL]: 47.25 % 	 [Task-IL]: 75.38 %
Accuracy for 9 task(s): 	 [Class-IL]: 43.14 % 	 [Task-IL]: 74.44 %
Accuracy for 10 task(s): 	 [Class-IL]: 40.68 % 	 [Task-IL]: 74.32 %
Accuracy for 10 task(s): 	 [Class-IL]: 40.07 % 	 [Task-IL]: 73.48 %
Accuracy for 10 task(s): 	 [Class-IL]: 40.77 % 	 [Task-IL]: 74.63 %
Accuracy for 10 task(s): 	 [Class-IL]: 40.78 % 	 [Task-IL]: 74.11 %
Accuracy for 10 task(s): 	 [Class-IL]: 40.27 % 	 [Task-IL]: 74.0 %

>   Average: 40.514/74.108





