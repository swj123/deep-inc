# GEM

> Title: Gradient Episodic Memory for Continual Learning
>
> Conference: NIPS2017
>
> URL: [Gradient Episodic Memory for Continual Learning (neurips.cc)](https://proceedings.neurips.cc/paper/2017/hash/f87522788a2be2d171666752f97ddebb-Abstract.html)

```
@inproceedings{NIPS2017_f8752278,
 author = {Lopez-Paz, David and Ranzato, Marc\textquotesingle Aurelio},
 booktitle = {Advances in Neural Information Processing Systems},
 title = {Gradient Episodic Memory for Continual Learning},
 volume = {30},
 year = {2017}
}
```

### CIFAR10

### buffer_size = 200

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.buffer_size = 200
args.n_epochs = 50

args.lr = 0.03
args.batch_size = 32
args.gamma = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.25 % 	 [Task-IL]: 98.25 %
Accuracy for 2 task(s): 	 [Class-IL]: 51.65 % 	 [Task-IL]: 93.48 %
Accuracy for 3 task(s): 	 [Class-IL]: 34.58 % 	 [Task-IL]: 90.48 %
Accuracy for 4 task(s): 	 [Class-IL]: 31.16 % 	 [Task-IL]: 85.94 %
Accuracy for 5 task(s): 	 [Class-IL]: 24.83 % 	 [Task-IL]: 86.8 %
Accuracy for 5 task(s): 	 [Class-IL]: 24.88 % 	 [Task-IL]: 85.87 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.23 % 	 [Task-IL]: 86.89 %
Accuracy for 5 task(s): 	 [Class-IL]: 24.97 % 	 [Task-IL]: 88.12 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.17 % 	 [Task-IL]: 85.31 %

>   Average：24.216 / 86.598

### buffer_size = 500

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.buffer_size = 500
args.n_epochs = 50

args.lr = 0.03
args.batch_size = 32
args.gamma = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.05 % 	 [Task-IL]: 99.05 %
Accuracy for 2 task(s): 	 [Class-IL]: 55.32 % 	 [Task-IL]: 89.35 %
Accuracy for 3 task(s): 	 [Class-IL]: 35.62 % 	 [Task-IL]: 89.03 %
Accuracy for 4 task(s): 	 [Class-IL]: 30.49 % 	 [Task-IL]: 84.69 %
Accuracy for 5 task(s): 	 [Class-IL]: 24.06 % 	 [Task-IL]: 86.46 %
Accuracy for 5 task(s): 	 [Class-IL]: 25.83 % 	 [Task-IL]: 82.1 %
Accuracy for 5 task(s): 	 [Class-IL]: 22.59 % 	 [Task-IL]: 83.9 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.18 % 	 [Task-IL]: 85.12 %
Accuracy for 5 task(s): 	 [Class-IL]: 22.63 % 	 [Task-IL]: 88.11 %

>   Average: 23.658 / 85.138

### buffer_size = 5120

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.buffer_size = 5120
args.n_epochs = 50

args.lr = 0.03
args.batch_size = 32
args.gamma = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.35 % 	 [Task-IL]: 98.35 %
Accuracy for 2 task(s): 	 [Class-IL]: 54.2 % 	 [Task-IL]: 92.92 %
Accuracy for 3 task(s): 	 [Class-IL]: 35.17 % 	 [Task-IL]: 89.93 %
Accuracy for 4 task(s): 	 [Class-IL]: 30.84 % 	 [Task-IL]: 88.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.72 % 	 [Task-IL]: 86.48 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.88 % 	 [Task-IL]: 86.52 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.29 % 	 [Task-IL]: 85.59 %
Accuracy for 5 task(s): 	 [Class-IL]: 24.84 % 	 [Task-IL]: 86.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 28.18 % 	 [Task-IL]: 85.32 %

>   Average: 24.382/ 85.982

### MNIST

### buffer_size = 200

```
# args.dataset = 'seq-mnist'
# args.buffer_size = 200
# args.print_freq = 10
# args.n_epochs = 10
#
# args.lr = 0.03
# args.batch_size = 32
# args.gamma = 2
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 95.24 % 	 [Task-IL]: 99.54 %
Accuracy for 3 task(s): 	 [Class-IL]: 82.14 % 	 [Task-IL]: 99.01 %
Accuracy for 4 task(s): 	 [Class-IL]: 76.01 % 	 [Task-IL]: 98.53 %
Accuracy for 5 task(s): 	 [Class-IL]: 71.51 % 	 [Task-IL]: 98.74 %
Accuracy for 5 task(s): 	 [Class-IL]: 72.8 % 	 [Task-IL]: 98.68 %
Accuracy for 5 task(s): 	 [Class-IL]: 70.81 % 	 [Task-IL]: 98.09 %
Accuracy for 5 task(s): 	 [Class-IL]: 69.55 % 	 [Task-IL]: 98.45 %
Accuracy for 5 task(s): 	 [Class-IL]: 72.5 % 	 [Task-IL]: 98.3 %

> Average: 71.434 / 98.452

### buffer_size = 500

```
# args.dataset = 'seq-mnist'
# args.buffer_size = 500
# args.print_freq = 10
# args.n_epochs = 10
#
# args.lr = 0.03
# args.batch_size = 32
# args.gamma = 2
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.91 % 	 [Task-IL]: 99.91 %
Accuracy for 2 task(s): 	 [Class-IL]: 96.63 % 	 [Task-IL]: 98.22 %
Accuracy for 3 task(s): 	 [Class-IL]: 92.9 % 	 [Task-IL]: 97.86 %
Accuracy for 4 task(s): 	 [Class-IL]: 90.73 % 	 [Task-IL]: 98.09 %
Accuracy for 5 task(s): 	 [Class-IL]: 86.69 % 	 [Task-IL]: 97.98 %
Accuracy for 5 task(s): 	 [Class-IL]: 86.21 % 	 [Task-IL]: 97.73 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.06 % 	 [Task-IL]: 97.59 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.16 % 	 [Task-IL]: 97.84 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.1 % 	 [Task-IL]: 97.58 %

> Average: 85.644 / 97.744

### buffer_size = 5120

```
# args.dataset = 'seq-mnist'
# args.buffer_size = 5120
# args.print_freq = 10
# args.n_epochs = 10
#
# args.lr = 0.03
# args.batch_size = 32
# args.gamma = 2
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 96.78 % 	 [Task-IL]: 98.41 %
Accuracy for 3 task(s): 	 [Class-IL]: 91.66 % 	 [Task-IL]: 97.23 %
Accuracy for 4 task(s): 	 [Class-IL]: 89.73 % 	 [Task-IL]: 97.38 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.77 % 	 [Task-IL]: 97.54 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.55 % 	 [Task-IL]: 97.47 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.83 % 	 [Task-IL]: 97.78 %
Accuracy for 5 task(s): 	 [Class-IL]: 86.07 % 	 [Task-IL]: 97.65 %
Accuracy for 5 task(s): 	 [Class-IL]: 87.3 % 	 [Task-IL]: 97.75 %

> Average: 85.90 / 97.638

### CIFAR100

### buffer_size = 200

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.buffer_size = 200
args.n_epochs = 50

args.lr = 0.03
args.batch_size = 32
args.gamma = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 79.3 % 	 [Task-IL]: 79.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 41.1 % 	 [Task-IL]: 67.7 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.23 % 	 [Task-IL]: 69.8 %
Accuracy for 4 task(s): 	 [Class-IL]: 22.48 % 	 [Task-IL]: 63.15 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.14 % 	 [Task-IL]: 64.44 %
Accuracy for 6 task(s): 	 [Class-IL]: 17.0 % 	 [Task-IL]: 60.17 %
Accuracy for 7 task(s): 	 [Class-IL]: 15.44 % 	 [Task-IL]: 60.79 %
Accuracy for 8 task(s): 	 [Class-IL]: 11.65 % 	 [Task-IL]: 51.89 %
Accuracy for 9 task(s): 	 [Class-IL]: 11.04 % 	 [Task-IL]: 55.59 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.45 % 	 [Task-IL]: 64.72 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.65 % 	 [Task-IL]: 58.42 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.99 % 	 [Task-IL]: 50.18 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.25 % 	 [Task-IL]: 63.03 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.8 % 	 [Task-IL]: 64.8 %

>   Average：10.828 / 60.23

### buffer_size = 500

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.buffer_size = 500
args.n_epochs = 50

args.lr = 0.03
args.batch_size = 32
args.gamma = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 75.2 % 	 [Task-IL]: 75.2 %
Accuracy for 2 task(s): 	 [Class-IL]: 39.25 % 	 [Task-IL]: 67.15 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.83 % 	 [Task-IL]: 69.03 %
Accuracy for 4 task(s): 	 [Class-IL]: 25.37 % 	 [Task-IL]: 64.78 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.88 % 	 [Task-IL]: 63.32 %
Accuracy for 6 task(s): 	 [Class-IL]: 18.1 % 	 [Task-IL]: 62.7 %
Accuracy for 7 task(s): 	 [Class-IL]: 14.66 % 	 [Task-IL]: 57.79 %
Accuracy for 8 task(s): 	 [Class-IL]: 14.36 % 	 [Task-IL]: 59.74 %
Accuracy for 9 task(s): 	 [Class-IL]: 13.57 % 	 [Task-IL]: 61.81 %
Accuracy for 10 task(s): 	 [Class-IL]: 12.38 % 	 [Task-IL]: 64.33 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.66 % 	 [Task-IL]: 61.95 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.88 % 	 [Task-IL]: 62.55 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.46 % 	 [Task-IL]: 58.4 %
Accuracy for 10 task(s): 	 [Class-IL]: 12.4 % 	 [Task-IL]: 66.75 %

>   Average: 11.756 / 62.796

### buffer_size = 5120

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.buffer_size = 5120
args.n_epochs = 50

args.lr = 0.03
args.batch_size = 32
args.gamma = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 79.1 % 	 [Task-IL]: 79.1 %
Accuracy for 2 task(s): 	 [Class-IL]: 38.55 % 	 [Task-IL]: 63.35 %
Accuracy for 3 task(s): 	 [Class-IL]: 28.77 % 	 [Task-IL]: 65.07 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.12 % 	 [Task-IL]: 66.27 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.1 % 	 [Task-IL]: 66.14 %
Accuracy for 6 task(s): 	 [Class-IL]: 13.88 % 	 [Task-IL]: 51.47 %
Accuracy for 7 task(s): 	 [Class-IL]: 15.6 % 	 [Task-IL]: 62.83 %
Accuracy for 8 task(s): 	 [Class-IL]: 13.2 % 	 [Task-IL]: 61.8 %
Accuracy for 9 task(s): 	 [Class-IL]: 13.67 % 	 [Task-IL]: 62.76 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.2 % 	 [Task-IL]: 57.25 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.8 % 	 [Task-IL]: 63.39 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.57 % 	 [Task-IL]: 62.19 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.13 % 	 [Task-IL]: 61.95 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.93 % 	 [Task-IL]: 51.25 %

>   Average: 10.926 / 59.206
