# DeepInversion

> Title: Dreaming to Distill: Data-Free Knowledge Transfer via DeepInversion
>
> Conference: CVPR2020
>
> URL: [CVPR 2020 Open Access Repository (thecvf.com)](https://openaccess.thecvf.com/content_CVPR_2020/html/Yin_Dreaming_to_Distill_Data-Free_Knowledge_Transfer_via_DeepInversion_CVPR_2020_paper.html)

```
@InProceedings{Yin_2020_CVPR,
author = {Yin, Hongxu and Molchanov, Pavlo and Alvarez, Jose M. and Li, Zhizhong and Mallya, Arun and Hoiem, Derek and Jha, Niraj K. and Kautz, Jan},
title = {Dreaming to Distill: Data-Free Knowledge Transfer via DeepInversion},
booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)},
month = {June},
year = {2020}
}
```

### CIFAR100

```
# args.dataset = 'seq-cifar100'
# args.print_freq = 10
# args.n_epochs = 100  # 不变
#
# args.lr = 0.05
# args.batch_size = 32
# args.power_iters = 10000
# args.mu = 0.5
# args.deep_inv_params = [1e-3, 5e1, 1e-3, 1e3, 1]
```

Accuracy for 1 task(s): 	 [Class-IL]: 89.2 % 	 [Task-IL]: 89.2 %
Accuracy for 2 task(s): 	 [Class-IL]: 46.35 % 	 [Task-IL]: 87.25 %
Accuracy for 3 task(s): 	 [Class-IL]: 32.2 % 	 [Task-IL]: 84.07 %
Accuracy for 4 task(s): 	 [Class-IL]: 21.95 % 	 [Task-IL]: 74.82 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.04 % 	 [Task-IL]: 77.16 %
Accuracy for 6 task(s): 	 [Class-IL]: 17.03 % 	 [Task-IL]: 74.47 %
Accuracy for 7 task(s): 	 [Class-IL]: 16.19 % 	 [Task-IL]: 71.7 %
Accuracy for 8 task(s): 	 [Class-IL]: 12.85 % 	 [Task-IL]: 70.19 %
Accuracy for 9 task(s): 	 [Class-IL]: 12.7 % 	 [Task-IL]: 67.6 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.04 % 	 [Task-IL]: 71.95 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.08 % 	 [Task-IL]: 68.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.57 % 	 [Task-IL]: 68.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.88 % 	 [Task-IL]: 66.09 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.39 % 	 [Task-IL]: 66.48 %

>   Average: 11.192 / 68.43

### CIFAR10

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.n_epochs = 100

args.lr = 0.005
args.batch_size = 32
args.power_iters = 10000
args.mu = 0.5
args.deep_inv_params = [1e-3, 5e1, 1e-3, 1e3, 1]
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.15 % 	 [Task-IL]: 98.5 %
Accuracy for 2 task(s): 	 [Class-IL]: 54.65 % 	 [Task-IL]: 95.1 %
Accuracy for 3 task(s): 	 [Class-IL]: 33.27 % 	 [Task-IL]: 94.88 %
Accuracy for 4 task(s): 	 [Class-IL]: 25.17 % 	 [Task-IL]: 95.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 26.44 % 	 [Task-IL]: 94.82 %
Accuracy for 5 task(s): 	 [Class-IL]: 25.18 % 	 [Task-IL]: 94.85 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.76 % 	 [Task-IL]: 94.14 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.19 % 	 [Task-IL]: 93.44 %
Accuracy for 5 task(s): 	 [Class-IL]: 26.14 % 	 [Task-IL]: 95.04 %

>   Average: 24.142 / 94.458

### MNIST

```
args.dataset = 'seq-mnist'
args.print_freq = 10
args.n_epochs = 10

args.lr = 5e-4      #ok
args.batch_size = 32 #ok
args.power_iters = 1000
args.mu = 0.1
args.deep_inv_params = [1e-3, 0, 1e-3, 1e3, 1]
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.53 % 	 [Task-IL]: 99.91 %
Accuracy for 2 task(s): 	 [Class-IL]: 47.2 % 	 [Task-IL]: 96.9 %
Accuracy for 3 task(s): 	 [Class-IL]: 29.86 % 	 [Task-IL]: 96.68 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.57 % 	 [Task-IL]: 96.12 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.52 % 	 [Task-IL]: 94.7 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.87 % 	 [Task-IL]: 96.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.8 % 	 [Task-IL]: 95.83 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.53 % 	 [Task-IL]: 96.22 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.74 % 	 [Task-IL]: 94.37 %

>   Average: 20.692 / 95.424

### Tiny-ImageNet

    args.dataset = 'seq-tinyimg'
    args.print_freq = 10
    args.n_epochs = 100
    
    args.lr = 0.1
    args.batch_size = 16
    args.power_iters = 10000
    args.mu = 5
    args.deep_inv_params = [1e-3, 5e1, 1e-3, 1e3, 1]

Accuracy for 1 task(s): 	 [Class-IL]: 76.8 % 	 [Task-IL]: 76.8 %
Accuracy for 2 task(s): 	 [Class-IL]: 38.1 % 	 [Task-IL]: 71.4 %
Accuracy for 3 task(s): 	 [Class-IL]: 25.6 % 	 [Task-IL]: 71.6 %
Accuracy for 4 task(s): 	 [Class-IL]: 21.42 % 	 [Task-IL]: 71.45 %
Accuracy for 5 task(s): 	 [Class-IL]: 17.22 % 	 [Task-IL]: 70.94 %
Accuracy for 6 task(s): 	 [Class-IL]: 13.25 % 	 [Task-IL]: 69.98 %
Accuracy for 7 task(s): 	 [Class-IL]: 11.96 % 	 [Task-IL]: 69.66 %
Accuracy for 8 task(s): 	 [Class-IL]: 10.79 % 	 [Task-IL]: 68.61 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.42 % 	 [Task-IL]: 66.73 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.25 % 	 [Task-IL]: 65.4 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.3 % 	 [Task-IL]: 66.5 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.78 % 	 [Task-IL]: 66.54 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.27 % 	 [Task-IL]: 67.38 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.35 % 	 [Task-IL]: 64.77 %

> Average: 8.19 / 66.118
