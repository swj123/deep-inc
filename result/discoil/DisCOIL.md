# DisCOIL

> Title: Exemplar-free Class Incremental Learning via Discriminative and Comparable One-class Classifiers
>
> Journal: arxiv2022
>
> Url: [[2201.01488\] Exemplar-free Class Incremental Learning via Discriminative and Comparable One-class Classifiers (arxiv.org)](https://arxiv.org/abs/2201.01488)

```
@article{sun2022exemplar,
  title={Exemplar-free Class Incremental Learning via Discriminative and Comparable One-class Classifiers},
  author={Sun, Wenju and Li, Qingyong and Zhang, Jing and Wang, Danyu and Wang, Wen and Geng, Yangli-ao},
  journal={arXiv preprint arXiv:2201.01488},
  year={2022}
}
```

#### MNIST

```
args.dataset = 'seq-mnist'
args.lr = 2e-3         
args.batch_size = 16
args.n_epochs = 10

args.eta = 1           
args.prato = 0.8       
args.kld_ratio = 0      
args.eps = 1
args.embedding_dim = 70 
args.weight_decay = 1e-3 
args.margin = 2        
args.neg_margin = 50    
args.r = 1              
args.nf = 64
args.isPrint = True        
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.92 % 	 [Task-IL]: 99.92 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.8 % 	 [Task-IL]: 99.38 %
Accuracy for 3 task(s): 	 [Class-IL]: 98.09 % 	 [Task-IL]: 99.47 %
Accuracy for 4 task(s): 	 [Class-IL]: 97.73 % 	 [Task-IL]: 99.6 %
Accuracy for 5 task(s): 	 [Class-IL]: 96.76 % 	 [Task-IL]: 99.63 %
Accuracy for 5 task(s): 	 [Class-IL]: 96.63 % 	 [Task-IL]: 99.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 96.97 % 	 [Task-IL]: 99.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 96.58 % 	 [Task-IL]: 99.6 %
Accuracy for 5 task(s): 	 [Class-IL]: 96.5 % 	 [Task-IL]: 99.58 %

>   Average：96.69 / 99.61

### CIFAR10

```
args.dataset = 'seq-cifar10'
args.lr = 5e-4                      
args.batch_size = 8 
args.n_epochs = 50  

args.eta = 10                       
args.prato = 0                    
args.kld_ratio = 0 
args.eps = 1  
args.embedding_dim = 250  
args.weight_decay = 0.001  
args.margin = 0                  
args.negmargin = 500 
args.r = 20                       
```

Accuracy for 1 task(s): 	 [Class-IL]: 94.85 % 	 [Task-IL]: 94.85 %
Accuracy for 2 task(s): 	 [Class-IL]: 72.95 % 	 [Task-IL]: 88.98 %
Accuracy for 3 task(s): 	 [Class-IL]: 55.85 % 	 [Task-IL]: 88.37 %
Accuracy for 4 task(s): 	 [Class-IL]: 47.75 % 	 [Task-IL]: 90.16 %
Accuracy for 5 task(s): 	 [Class-IL]: 43.81 % 	 [Task-IL]: 90.81 %
Accuracy for 5 task(s): 	 [Class-IL]: 45.22 % 	 [Task-IL]: 90.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 43.6 % 	 [Task-IL]: 90.32 %
Accuracy for 5 task(s): 	 [Class-IL]: 44.54 % 	 [Task-IL]: 90.33 %
Accuracy for 5 task(s): 	 [Class-IL]: 44.32 % 	 [Task-IL]: 90.06 %

>   Average：44.298 / 90.428

#### CIFAR100

```
args.dataset = 'seq-cifar100'
args.lr = 5e-5                      
args.batch_size = 8 
args.n_epochs = 50  

args.lambda2 = 10                   
args.kld_ratio = 0 
args.eps = 1  
args.embedding_dim = 250  
args.weight_decay = 0.001  
args.lambda1 = 20                  
args.r_inter = 200 
args.r_intra = 10             
args.isPseudo = False
args.nf = 64   
```

Accuracy for 1 task(s): 	 [Class-IL]: 72.4 % 	 [Task-IL]: 72.4 %
Accuracy for 2 task(s): 	 [Class-IL]: 54.85 % 	 [Task-IL]: 70.55 %
Accuracy for 3 task(s): 	 [Class-IL]: 47.07 % 	 [Task-IL]: 70.77 %
Accuracy for 4 task(s): 	 [Class-IL]: 40.3 % 	 [Task-IL]: 70.45 %
Accuracy for 5 task(s): 	 [Class-IL]: 36.28 % 	 [Task-IL]: 70.72 %
Accuracy for 6 task(s): 	 [Class-IL]: 32.5 % 	 [Task-IL]: 71.22 %
Accuracy for 7 task(s): 	 [Class-IL]: 31.11 % 	 [Task-IL]: 72.11 %
Accuracy for 8 task(s): 	 [Class-IL]: 29.18 % 	 [Task-IL]: 71.92 %
Accuracy for 9 task(s): 	 [Class-IL]: 27.39 % 	 [Task-IL]: 72.04 %
Accuracy for 10 task(s): 	 [Class-IL]: 25.76 % 	 [Task-IL]: 72.35 %
Accuracy for 10 task(s): 	 [Class-IL]: 25.53 % 	 [Task-IL]: 71.92 %
Accuracy for 10 task(s): 	 [Class-IL]: 26.54 % 	 [Task-IL]: 72.41 %
Accuracy for 10 task(s): 	 [Class-IL]: 25.82 % 	 [Task-IL]: 72.19 %
Accuracy for 10 task(s): 	 [Class-IL]: 25.34 % 	 [Task-IL]: 72.13 %

>   Average: 25.798 / 72.2

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.lr = 5e-5  # ok            
args.batch_size = 32         
args.n_epochs = 100  

args.eta = 10       
args.prato = 0  
args.kld_ratio = 0  
args.eps = 1  
args.embedding_dim = 250  
args.weight_decay = 1e-2             
args.margin = 0.01                      
args.neg_margin = 700               
args.r = 0  # ?? ok                    

args.nf = 64
args.isPrint = True
```

Accuracy for 1 task(s): 	 [Class-IL]: 57.6 % 	 [Task-IL]: 57.6 %
Accuracy for 2 task(s): 	 [Class-IL]: 41.45 % 	 [Task-IL]: 52.7 %
Accuracy for 3 task(s): 	 [Class-IL]: 34.7 % 	 [Task-IL]: 52.73 %
Accuracy for 4 task(s): 	 [Class-IL]: 32.08 % 	 [Task-IL]: 53.65 %
Accuracy for 5 task(s): 	 [Class-IL]: 29.56 % 	 [Task-IL]: 54.86 %
Accuracy for 6 task(s): 	 [Class-IL]: 26.45 % 	 [Task-IL]: 54.53 %
Accuracy for 7 task(s): 	 [Class-IL]: 24.17 % 	 [Task-IL]: 54.26 %
Accuracy for 8 task(s): 	 [Class-IL]: 22.7 % 	 [Task-IL]: 53.9 %
Accuracy for 9 task(s): 	 [Class-IL]: 20.9 % 	 [Task-IL]: 52.69 %
Accuracy for 10 task(s): 	 [Class-IL]: 19.65 % 	 [Task-IL]: 52.68 %
Accuracy for 10 task(s): 	 [Class-IL]: 19.72 % 	 [Task-IL]: 53.17 %
Accuracy for 10 task(s): 	 [Class-IL]: 19.95 % 	 [Task-IL]: 53.16 %
Accuracy for 10 task(s): 	 [Class-IL]: 19.96 % 	 [Task-IL]: 53.31 %

>   Average：19.746/ 53.08

