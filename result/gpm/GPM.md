# GPM

> Title: Learning without forgetting
>
> Conference: ICLR2021 oral
>
> Url: [Gradient Projection Memory for Continual Learning | OpenReview](https://openreview.net/forum?id=3AOj0RCNC2)

```
@inproceedings{gpm,
title={Gradient projection memory for continual learning},
author={Gobinda Saha and Isha Garg and Kaushik Roy},
booktitle={International Conference on Learning Representations},
year={2021},
}
```

### MNIST

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10  # 不变

args.lr = 0.02
args.batch_size = 32
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.59 % 	 [Task-IL]: 99.37 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.94 % 	 [Task-IL]: 98.59 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.71 % 	 [Task-IL]: 96.59 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.63 % 	 [Task-IL]: 95.39 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.58 % 	 [Task-IL]: 91.39 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.64 % 	 [Task-IL]: 93.52 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.62 % 	 [Task-IL]: 95.79 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.63 % 	 [Task-IL]: 90.42 %

>   Average：19.62 / 93.3

### CIFAR10

```
# args.dataset = 'seq-cifar10'
# args.print_freq = 10
# args.n_epochs = 50
# args.pro_exp_size = 200
#
# args.lr = 0.02
# args.batch_size = 64
```

Accuracy for 1 task(s): 	 [Class-IL]: 97.25 % 	 [Task-IL]: 97.25 %
Accuracy for 2 task(s): 	 [Class-IL]: 42.98 % 	 [Task-IL]: 89.98 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.27 % 	 [Task-IL]: 87.93 %
Accuracy for 4 task(s): 	 [Class-IL]: 23.94 % 	 [Task-IL]: 87.3 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.84 % 	 [Task-IL]: 86.5 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.01 % 	 [Task-IL]: 87.32 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.95 % 	 [Task-IL]: 84.77 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.74 % 	 [Task-IL]: 85.21 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.88 % 	 [Task-IL]: 86.18 %

>   Average: 18.884 / 85.996

#### CIFAR100

```
args.dataset = 'seq-cifar100'
args.print_freq = 10
args.n_epochs = 100

args.lr = 0.01
args.batch_size = 64
```

Accuracy for 1 task(s): 	 [Class-IL]: 73.3 % 	 [Task-IL]: 73.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 36.7 % 	 [Task-IL]: 75.55 %
Accuracy for 3 task(s): 	 [Class-IL]: 25.73 % 	 [Task-IL]: 74.93 %
Accuracy for 4 task(s): 	 [Class-IL]: 19.05 % 	 [Task-IL]: 73.6 %
Accuracy for 5 task(s): 	 [Class-IL]: 15.54 % 	 [Task-IL]: 74.16 %
Accuracy for 6 task(s): 	 [Class-IL]: 12.83 % 	 [Task-IL]: 74.22 %
Accuracy for 7 task(s): 	 [Class-IL]: 11.23 % 	 [Task-IL]: 74.76 %
Accuracy for 8 task(s): 	 [Class-IL]: 8.62 % 	 [Task-IL]: 73.60 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.83 % 	 [Task-IL]: 74.47 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.26 % 	 [Task-IL]: 75.43 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.19 % 	 [Task-IL]: 75.48 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.15 % 	 [Task-IL]: 73.63 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.01 % 	 [Task-IL]: 74.08 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.33 % 	 [Task-IL]: 76.1 %

>   Average: 8.188 / 74.944

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100
args.pro_exp_size = 1000

args.lr = 0.02
args.batch_size = 32
```

Accuracy for 1 task(s): 	 [Class-IL]: 73.7 % 	 [Task-IL]: 73.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 32.25 % 	 [Task-IL]: 66.9 %
Accuracy for 3 task(s): 	 [Class-IL]: 22.67 % 	 [Task-IL]: 64.37 %
Accuracy for 4 task(s): 	 [Class-IL]: 17.97 % 	 [Task-IL]: 64.98 %
Accuracy for 5 task(s): 	 [Class-IL]: 14.24 % 	 [Task-IL]: 65.78 %
Accuracy for 6 task(s): 	 [Class-IL]: 11.35 % 	 [Task-IL]: 65.4 %
Accuracy for 7 task(s): 	 [Class-IL]: 9.77 % 	 [Task-IL]: 65.3 %
Accuracy for 8 task(s): 	 [Class-IL]: 8.62 % 	 [Task-IL]: 64.74 %
Accuracy for 9 task(s): 	 [Class-IL]: 6.86 % 	 [Task-IL]: 63.62 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.97 % 	 [Task-IL]: 63.72 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.04 % 	 [Task-IL]: 63.14 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.09 % 	 [Task-IL]: 63.51 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.05 % 	 [Task-IL]: 63.3 %

> Average: 7.04 / 63.41

