# DER

> Title: Dark Experience for General Continual Learning: a Strong, Simple Baseline
>
> Conference: Neurips2020
>
> Url: [Advances in Neural Information Processing Systems 33 (NeurIPS 2020)](https://proceedings.neurips.cc/paper/2020/hash/b704ea2c39778f07c617f6b7ce480e9e-Abstract.html)

```
@inproceedings{NEURIPS2020_b704ea2c,
 author = {Buzzega, Pietro and Boschini, Matteo and Porrello, Angelo and Abati, Davide and CALDERARA, SIMONE},
 booktitle = {Advances in Neural Information Processing Systems},
 editor = {H. Larochelle and M. Ranzato and R. Hadsell and M.F. Balcan and H. Lin},
 pages = {15920--15930},
 publisher = {Curran Associates, Inc.},
 title = {Dark Experience for General Continual Learning: a Strong, Simple Baseline},
 url = {https://proceedings.neurips.cc/paper/2020/file/b704ea2c39778f07c617f6b7ce480e9e-Paper.pdf},
 volume = {33},
 year = {2020}
}
}
```

#### CIFAR10

### buffer_size = 200

```
args.dataset = 'seq-cifar10'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 200
args.alpha = 0.3
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.8 % 	 [Task-IL]: 98.8 %
Accuracy for 2 task(s): 	 [Class-IL]: 80.18 % 	 [Task-IL]: 94.82 %
Accuracy for 3 task(s): 	 [Class-IL]: 55.18 % 	 [Task-IL]: 92.12 %
Accuracy for 4 task(s): 	 [Class-IL]: 42.58 % 	 [Task-IL]: 90.02 %
Accuracy for 5 task(s): 	 [Class-IL]: 41.45 % 	 [Task-IL]: 89.32 %
Accuracy for 5 task(s): 	 [Class-IL]: 44.54 % 	 [Task-IL]: 84.87 %
Accuracy for 5 task(s): 	 [Class-IL]: 45.04 % 	 [Task-IL]: 88.55 %
Accuracy for 5 task(s): 	 [Class-IL]: 47.85 % 	 [Task-IL]: 86.81 %
Accuracy for 5 task(s): 	 [Class-IL]: 42.59 % 	 [Task-IL]: 85.87 %

>   Average：44.294 / 87.084

### buffer_size = 500

```
args.dataset = 'seq-cifar10'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 500
args.alpha = 0.3
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.3 % 	 [Task-IL]: 98.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 83.35 % 	 [Task-IL]: 95.32 %
Accuracy for 3 task(s): 	 [Class-IL]: 62.42 % 	 [Task-IL]: 92.63 %
Accuracy for 4 task(s): 	 [Class-IL]: 58.01 % 	 [Task-IL]: 91.54 %
Accuracy for 5 task(s): 	 [Class-IL]: 53.09 % 	 [Task-IL]: 91.85 %
Accuracy for 5 task(s): 	 [Class-IL]: 53.68 % 	 [Task-IL]: 90.47 %
Accuracy for 5 task(s): 	 [Class-IL]: 54.45 % 	 [Task-IL]: 92.13 %
Accuracy for 5 task(s): 	 [Class-IL]: 56.43 % 	 [Task-IL]: 91.12 %
Accuracy for 5 task(s): 	 [Class-IL]: 55.16 % 	 [Task-IL]: 91.45 %

>   Average：54.562 / 91.404

### buffer_size = 5120

```
args.dataset = 'seq-cifar10'
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 5120
args.alpha = 0.3
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.65 % 	 [Task-IL]: 98.65 %
Accuracy for 2 task(s): 	 [Class-IL]: 87.0 % 	 [Task-IL]: 95.6 %
Accuracy for 3 task(s): 	 [Class-IL]: 77.03 % 	 [Task-IL]: 94.35 %
Accuracy for 4 task(s): 	 [Class-IL]: 74.41 % 	 [Task-IL]: 94.2 %
Accuracy for 5 task(s): 	 [Class-IL]: 75.44 % 	 [Task-IL]: 94.59 %
Accuracy for 5 task(s): 	 [Class-IL]: 73.86 % 	 [Task-IL]: 95.29 %
Accuracy for 5 task(s): 	 [Class-IL]: 73.68 % 	 [Task-IL]: 94.81 %
Accuracy for 5 task(s): 	 [Class-IL]: 73.25 % 	 [Task-IL]: 94.61 %
Accuracy for 5 task(s): 	 [Class-IL]: 74.54 % 	 [Task-IL]: 95.17 %

>   Average：74.154 / 94.894

#### MNIST

### buffer_size = 200

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 128
args.batch_size = 128
args.buffer_size = 200
args.alpha = 0.2
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.6 % 	 [Task-IL]: 99.54 %
Accuracy for 3 task(s): 	 [Class-IL]: 94.13 % 	 [Task-IL]: 99.17 %
Accuracy for 4 task(s): 	 [Class-IL]: 92.0 % 	 [Task-IL]: 98.86 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.76 % 	 [Task-IL]: 98.81 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.81 % 	 [Task-IL]: 99.14 %
Accuracy for 5 task(s): 	 [Class-IL]: 83.39 % 	 [Task-IL]: 98.86 %
Accuracy for 5 task(s): 	 [Class-IL]: 84.02 % 	 [Task-IL]: 98.4 %
Accuracy for 5 task(s): 	 [Class-IL]: 82.21 % 	 [Task-IL]: 98.93 %

>   Average：83.438 / 98.828

### buffer_size = 500

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10

args.lr = 0.1
args.minibatch_size = 128
args.batch_size = 128
args.buffer_size = 500
args.alpha = 1.0
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.63 % 	 [Task-IL]: 99.49 %
Accuracy for 3 task(s): 	 [Class-IL]: 97.28 % 	 [Task-IL]: 99.45 %
Accuracy for 4 task(s): 	 [Class-IL]: 93.91 % 	 [Task-IL]: 99.23 %
Accuracy for 5 task(s): 	 [Class-IL]: 89.73 % 	 [Task-IL]: 98.99 %
Accuracy for 5 task(s): 	 [Class-IL]: 91.06 % 	 [Task-IL]: 98.98 %
Accuracy for 5 task(s): 	 [Class-IL]: 89.64 % 	 [Task-IL]: 98.99 %
Accuracy for 5 task(s): 	 [Class-IL]: 89.45 % 	 [Task-IL]: 98.9 %
Accuracy for 5 task(s): 	 [Class-IL]: 91.9 % 	 [Task-IL]: 99.22 %

>   Average：90.356 / 99.016

### buffer_size = 5120

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10

args.lr = 0.1
args.minibatch_size = 128
args.batch_size = 128
args.buffer_size = 5120
args.alpha = 0.5
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.91 % 	 [Task-IL]: 99.91 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.99 % 	 [Task-IL]: 99.62 %
Accuracy for 3 task(s): 	 [Class-IL]: 97.81 % 	 [Task-IL]: 99.4 %
Accuracy for 4 task(s): 	 [Class-IL]: 97.09 % 	 [Task-IL]: 99.46 %
Accuracy for 5 task(s): 	 [Class-IL]: 94.8 % 	 [Task-IL]: 99.37 %
Accuracy for 5 task(s): 	 [Class-IL]: 95.74 % 	 [Task-IL]: 99.35 %
Accuracy for 5 task(s): 	 [Class-IL]: 95.46 % 	 [Task-IL]: 99.36 %
Accuracy for 5 task(s): 	 [Class-IL]: 95.76 % 	 [Task-IL]: 99.47 %
Accuracy for 5 task(s): 	 [Class-IL]: 95.47 % 	 [Task-IL]: 99.4 %

>   Average：95.446 / 99.39

#### TINYIMAGE

### buffer_size = 200

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 200
args.alpha = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 81.1 % 	 [Task-IL]: 81.1 %
Accuracy for 2 task(s): 	 [Class-IL]: 44.5 % 	 [Task-IL]: 64.0 %
Accuracy for 3 task(s): 	 [Class-IL]: 29.27 % 	 [Task-IL]: 56.93 %
Accuracy for 4 task(s): 	 [Class-IL]: 23.88 % 	 [Task-IL]: 54.35 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.22 % 	 [Task-IL]: 50.1 %
Accuracy for 6 task(s): 	 [Class-IL]: 14.8 % 	 [Task-IL]: 49.17 %
Accuracy for 7 task(s): 	 [Class-IL]: 12.49 % 	 [Task-IL]: 44.34 %
Accuracy for 8 task(s): 	 [Class-IL]: 10.78 % 	 [Task-IL]: 41.09 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.99 % 	 [Task-IL]: 37.38 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.43 % 	 [Task-IL]: 34.93 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.25 % 	 [Task-IL]: 37.05 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.03 % 	 [Task-IL]: 36.52 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.65 % 	 [Task-IL]: 34.39 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.29 % 	 [Task-IL]: 37.43 %

>   Average：9.13 / 36.064

### buffer_size = 500

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 500
args.alpha = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 80.0 % 	 [Task-IL]: 80.0 %
Accuracy for 2 task(s): 	 [Class-IL]: 51.4 % 	 [Task-IL]: 67.8 %
Accuracy for 3 task(s): 	 [Class-IL]: 37.37 % 	 [Task-IL]: 63.0 %
Accuracy for 4 task(s): 	 [Class-IL]: 30.48 % 	 [Task-IL]: 59.65 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.2 % 	 [Task-IL]: 57.66 %
Accuracy for 6 task(s): 	 [Class-IL]: 18.07 % 	 [Task-IL]: 56.3 %
Accuracy for 7 task(s): 	 [Class-IL]: 14.89 % 	 [Task-IL]: 53.11 %
Accuracy for 8 task(s): 	 [Class-IL]: 12.58 % 	 [Task-IL]: 49.08 %
Accuracy for 9 task(s): 	 [Class-IL]: 10.43 % 	 [Task-IL]: 47.22 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.71 % 	 [Task-IL]: 47.34 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.94 % 	 [Task-IL]: 47.71 %
Accuracy for 10 task(s): 	 [Class-IL]: 12.36 % 	 [Task-IL]: 45.14 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.0 % 	 [Task-IL]: 45.91 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.78 % 	 [Task-IL]: 45.06 %

>   Average：10.758 / 46.232

### buffer_size = 5120

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 5120
args.alpha = 0.1
```


Accuracy for 1 task(s): 	 [Class-IL]: 79.1 % 	 [Task-IL]: 79.1 %
Accuracy for 2 task(s): 	 [Class-IL]: 66.7 % 	 [Task-IL]: 75.2 %
Accuracy for 3 task(s): 	 [Class-IL]: 59.47 % 	 [Task-IL]: 74.3 %
Accuracy for 4 task(s): 	 [Class-IL]: 55.25 % 	 [Task-IL]: 73.55 %
Accuracy for 5 task(s): 	 [Class-IL]: 42.36 % 	 [Task-IL]: 69.06 %
Accuracy for 6 task(s): 	 [Class-IL]: 44.05 % 	 [Task-IL]: 70.95 %
Accuracy for 7 task(s): 	 [Class-IL]: 42.04 % 	 [Task-IL]: 69.99 %
Accuracy for 8 task(s): 	 [Class-IL]: 39.85 % 	 [Task-IL]: 69.82 %
Accuracy for 9 task(s): 	 [Class-IL]: 35.54 % 	 [Task-IL]: 67.17 %
Accuracy for 10 task(s): 	 [Class-IL]: 32.05 % 	 [Task-IL]: 66.94 %
Accuracy for 10 task(s): 	 [Class-IL]: 33.59 % 	 [Task-IL]: 66.73 %
Accuracy for 10 task(s): 	 [Class-IL]: 33.34 % 	 [Task-IL]: 67.52 %
Accuracy for 10 task(s): 	 [Class-IL]: 33.5 % 	 [Task-IL]: 66.75 %
Accuracy for 10 task(s): 	 [Class-IL]: 32.76 % 	 [Task-IL]: 66.3 %

>   Average：33.048 / 66.848

#### CIFAR100

### buffer_size = 200

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 200
args.alpha = 0.1
```


Accuracy for 1 task(s): 	 [Class-IL]: 84.3 % 	 [Task-IL]: 84.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.6 % 	 [Task-IL]: 76.25 %
Accuracy for 3 task(s): 	 [Class-IL]: 40.9 % 	 [Task-IL]: 74.3 %
Accuracy for 4 task(s): 	 [Class-IL]: 27.92 % 	 [Task-IL]: 68.58 %
Accuracy for 5 task(s): 	 [Class-IL]: 24.44 % 	 [Task-IL]: 69.6 %
Accuracy for 6 task(s): 	 [Class-IL]: 20.77 % 	 [Task-IL]: 64.4 %
Accuracy for 7 task(s): 	 [Class-IL]: 20.09 % 	 [Task-IL]: 62.1 %
Accuracy for 8 task(s): 	 [Class-IL]: 15.96 % 	 [Task-IL]: 62.2 %
Accuracy for 9 task(s): 	 [Class-IL]: 12.72 % 	 [Task-IL]: 60.39 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.63 % 	 [Task-IL]: 59.12 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.35 % 	 [Task-IL]: 58.23 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.37 % 	 [Task-IL]: 58.83 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.91 % 	 [Task-IL]: 56.52 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.27 % 	 [Task-IL]: 58.5 %

>   Average：11.306 / 58.24

### buffer_size = 500

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 500
args.alpha = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 60.7 % 	 [Task-IL]: 60.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 57.05 % 	 [Task-IL]: 81.2 %
Accuracy for 3 task(s): 	 [Class-IL]: 44.97 % 	 [Task-IL]: 78.73 %
Accuracy for 4 task(s): 	 [Class-IL]: 40.02 % 	 [Task-IL]: 76.45 %
Accuracy for 5 task(s): 	 [Class-IL]: 28.04 % 	 [Task-IL]: 70.0 %
Accuracy for 6 task(s): 	 [Class-IL]: 26.87 % 	 [Task-IL]: 71.05 %
Accuracy for 7 task(s): 	 [Class-IL]: 24.81 % 	 [Task-IL]: 71.3 %
Accuracy for 8 task(s): 	 [Class-IL]: 21.02 % 	 [Task-IL]: 70.28 %
Accuracy for 9 task(s): 	 [Class-IL]: 19.54 % 	 [Task-IL]: 68.51 %
Accuracy for 10 task(s): 	 [Class-IL]: 17.69 % 	 [Task-IL]: 70.18 %
Accuracy for 10 task(s): 	 [Class-IL]: 18.38 % 	 [Task-IL]: 66.79 %
Accuracy for 10 task(s): 	 [Class-IL]: 16.13 % 	 [Task-IL]: 66.43 %
Accuracy for 10 task(s): 	 [Class-IL]: 20.3 % 	 [Task-IL]: 71.14 %
Accuracy for 10 task(s): 	 [Class-IL]: 16.56 % 	 [Task-IL]: 69.04 %

>   Average：17.812 / 68.716

### buffer_size = 5120

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.buffer_size = 5120
args.alpha = 0.1

```

Accuracy for 1 task(s): 	 [Class-IL]: 87.8 % 	 [Task-IL]: 87.8 %
Accuracy for 2 task(s): 	 [Class-IL]: 30.1 % 	 [Task-IL]: 52.1 %
Accuracy for 3 task(s): 	 [Class-IL]: 53.03 % 	 [Task-IL]: 73.2 %
Accuracy for 4 task(s): 	 [Class-IL]: 59.03 % 	 [Task-IL]: 83.72 %
Accuracy for 5 task(s): 	 [Class-IL]: 53.96 % 	 [Task-IL]: 83.88 %
Accuracy for 6 task(s): 	 [Class-IL]: 51.07 % 	 [Task-IL]: 82.93 %
Accuracy for 7 task(s): 	 [Class-IL]: 51.29 % 	 [Task-IL]: 83.14 %
Accuracy for 8 task(s): 	 [Class-IL]: 49.31 % 	 [Task-IL]: 83.01 %
Accuracy for 9 task(s): 	 [Class-IL]: 46.62 % 	 [Task-IL]: 82.97 %
Accuracy for 10 task(s): 	 [Class-IL]: 44.48 % 	 [Task-IL]: 82.77 %
Accuracy for 10 task(s): 	 [Class-IL]: 46.3 % 	 [Task-IL]: 83.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 45.82 % 	 [Task-IL]: 84.06 %
Accuracy for 10 task(s): 	 [Class-IL]: 40.4 % 	 [Task-IL]: 82.0 %
Accuracy for 10 task(s): 	 [Class-IL]: 47.32 % 	 [Task-IL]: 84.85 %

>   Average：44.864/ 83.484

