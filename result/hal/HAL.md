# HAL

> Title: Using Hindsight to Anchor Past Knowledge in Continual Learning
>
> Conference: AAAI2021 
>
> Url: [Using Hindsight to Anchor Past Knowledge in Continual Learning](https://ojs.aaai.org/index.php/AAAI/article/view/16861)

```
@article{Chaudhry_Gordo_Dokania_Torr_Lopez-Paz_2021, 
    title={Using Hindsight to Anchor Past Knowledge in Continual Learning}, 
    volume={35}, 
    journal={AAAI}, 
    author={Chaudhry, Arslan and Gordo, Albert and Dokania, Puneet and Torr, Philip and Lopez-Paz, David},
    year={2021}
}
```

### CIFAR10

### buffer_size = 200

```
args.dataset = 'seq-cifar10'
args.nt = 5
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.hal_lambda = 0.2
args.beta = 0.5
args.gamma = 0.1
args.buffer_size = 200
```

Accuracy for 1 task(s): 	 [Class-IL]: 96.65 % 	 [Task-IL]: 96.65 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.9 % 	 [Task-IL]: 83.55 %
Accuracy for 3 task(s): 	 [Class-IL]: 24.08 % 	 [Task-IL]: 69.93 %
Accuracy for 4 task(s): 	 [Class-IL]: 19.28 % 	 [Task-IL]: 74.29 %
Accuracy for 5 task(s): 	 [Class-IL]: 17.24 % 	 [Task-IL]: 66.05 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.3 % 	 [Task-IL]: 79.15 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.04 % 	 [Task-IL]: 76.02 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.28 % 	 [Task-IL]: 75.03 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.79 % 	 [Task-IL]: 80.53 %

>   Average：19.73 / 75.356

### buffer_size = 500

```
args.dataset = 'seq-cifar10'
args.nt = 5
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.hal_lambda = 0.1
args.beta = 0.3
args.gamma = 0.1
args.buffer_size = 500
```

Accuracy for 1 task(s): 	 [Class-IL]: 97.9 % 	 [Task-IL]: 97.9 %
Accuracy for 2 task(s): 	 [Class-IL]: 49.98 % 	 [Task-IL]: 86.05 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.47 % 	 [Task-IL]: 82.32 %
Accuracy for 4 task(s): 	 [Class-IL]: 22.36 % 	 [Task-IL]: 79.34 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.23 % 	 [Task-IL]: 73.29 %
Accuracy for 5 task(s): 	 [Class-IL]: 25.99 % 	 [Task-IL]: 79.32 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.79 % 	 [Task-IL]: 76.77 %
Accuracy for 5 task(s): 	 [Class-IL]: 28.01 % 	 [Task-IL]: 84.09 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.74 % 	 [Task-IL]: 76.58 %

>   Average：23.952 / 78.01

### buffer_size = 5120

```
args.dataset = 'seq-cifar10'
args.nt = 5
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.minibatch_size = 32
args.batch_size = 32
args.hal_lambda = 0.1
args.beta = 0.3
args.gamma = 0.1
args.buffer_size = 5120
```

Accuracy for 1 task(s): 	 [Class-IL]: 97.95 % 	 [Task-IL]: 97.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 59.5 % 	 [Task-IL]: 86.98 %
Accuracy for 3 task(s): 	 [Class-IL]: 43.87 % 	 [Task-IL]: 83.52 %
Accuracy for 4 task(s): 	 [Class-IL]: 43.92 % 	 [Task-IL]: 82.96 %
Accuracy for 5 task(s): 	 [Class-IL]: 45.45 % 	 [Task-IL]: 84.67 %
Accuracy for 5 task(s): 	 [Class-IL]: 49.3 % 	 [Task-IL]: 80.9 %
Accuracy for 5 task(s): 	 [Class-IL]: 44.91 % 	 [Task-IL]: 83.09 %
Accuracy for 5 task(s): 	 [Class-IL]: 39.08 % 	 [Task-IL]: 80.41 %
Accuracy for 5 task(s): 	 [Class-IL]: 43.92 % 	 [Task-IL]: 82.63 %

>   Average：44.532 / 82.34

### MNIST

### buffer_size = 200

```
args.dataset = 'seq-mnist'
args.nt = 5
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 128
args.batch_size = 128
args.hal_lambda = 0.1
args.beta = 0.7
args.gamma = 0.5
args.buffer_size = 200
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 97.16 % 	 [Task-IL]: 99.47 %
Accuracy for 3 task(s): 	 [Class-IL]: 89.19 % 	 [Task-IL]: 98.72 %
Accuracy for 4 task(s): 	 [Class-IL]: 86.09 % 	 [Task-IL]: 99.04 %
Accuracy for 5 task(s): 	 [Class-IL]: 79.41 % 	 [Task-IL]: 98.84 %
Accuracy for 5 task(s): 	 [Class-IL]: 78.44 % 	 [Task-IL]: 98.92 %
Accuracy for 5 task(s): 	 [Class-IL]: 76.84 % 	 [Task-IL]: 99.07 %
Accuracy for 5 task(s): 	 [Class-IL]: 80.63 % 	 [Task-IL]: 99.11 %
Accuracy for 5 task(s): 	 [Class-IL]: 79.96 % 	 [Task-IL]: 98.63 %

>   Average：79.056 / 98.914

### buffer_size = 500

```
args.dataset = 'seq-mnist'
args.nt = 5
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 128
args.batch_size = 128
args.hal_lambda = 0.1
args.beta = 0.2
args.gamma = 0.5
args.buffer_size = 500
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.86 % 	 [Task-IL]: 99.86 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.03 % 	 [Task-IL]: 99.25 %
Accuracy for 3 task(s): 	 [Class-IL]: 93.8 % 	 [Task-IL]: 99.3 %
Accuracy for 4 task(s): 	 [Class-IL]: 92.19 % 	 [Task-IL]: 99.25 %
Accuracy for 5 task(s): 	 [Class-IL]: 87.48 % 	 [Task-IL]: 99.1 %
Accuracy for 5 task(s): 	 [Class-IL]: 87.78 % 	 [Task-IL]: 99.05 %
Accuracy for 5 task(s): 	 [Class-IL]: 88.02 % 	 [Task-IL]: 99.19 %
Accuracy for 5 task(s): 	 [Class-IL]: 87.08 % 	 [Task-IL]: 99.03 %
Accuracy for 5 task(s): 	 [Class-IL]: 86.46 % 	 [Task-IL]: 99.2 %

>   Average：87.364 / 99.114

### buffer_size = 5120

```
args.dataset = 'seq-mnist'
args.nt = 5
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 128
args.batch_size = 128
args.hal_lambda = 0.1
args.beta = 0.7
args.gamma = 0.5
args.buffer_size = 5120
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.86 % 	 [Task-IL]: 99.86 %
Accuracy for 2 task(s): 	 [Class-IL]: 98.82 % 	 [Task-IL]: 99.42 %
Accuracy for 3 task(s): 	 [Class-IL]: 97.25 % 	 [Task-IL]: 99.52 %
Accuracy for 4 task(s): 	 [Class-IL]: 96.47 % 	 [Task-IL]: 99.46 %
Accuracy for 5 task(s): 	 [Class-IL]: 94.93 % 	 [Task-IL]: 99.56 %
Accuracy for 5 task(s): 	 [Class-IL]: 94.85 % 	 [Task-IL]: 99.44 %
Accuracy for 5 task(s): 	 [Class-IL]: 95.07 % 	 [Task-IL]: 99.48 %
Accuracy for 5 task(s): 	 [Class-IL]: 94.72 % 	 [Task-IL]: 99.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 95.0 % 	 [Task-IL]: 99.46 %

>   Average：94.914 / 99.512

### CIFAR100

### buffer size = 200

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 100

args.lr = 0.1   
args.minibatch_size = 32
args.batch_size = 32
args.hal_lambda = 0.1 
args.beta = 0.5
args.gamma = 1
args.buffer_size = 200
```

Accuracy for 1 task(s): 	 [Class-IL]: 81.2 % 	 [Task-IL]: 81.2 %
Accuracy for 2 task(s): 	 [Class-IL]: 34.75 % 	 [Task-IL]: 50.2 %
Accuracy for 3 task(s): 	 [Class-IL]: 26.63 % 	 [Task-IL]: 54.73 %
Accuracy for 4 task(s): 	 [Class-IL]: 21.02 % 	 [Task-IL]: 50.55 %
Accuracy for 5 task(s): 	 [Class-IL]: 12.52 % 	 [Task-IL]: 41.86 %
Accuracy for 6 task(s): 	 [Class-IL]: 12.65 % 	 [Task-IL]: 42.73 %
Accuracy for 7 task(s): 	 [Class-IL]: 10.01 % 	 [Task-IL]: 37.61 %
Accuracy for 8 task(s): 	 [Class-IL]: 8.79 % 	 [Task-IL]: 37.26 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.49 % 	 [Task-IL]: 38.57 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.39 % 	 [Task-IL]: 45.09 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.69 % 	 [Task-IL]: 39.3 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.26 % 	 [Task-IL]: 38.54 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.19 % 	 [Task-IL]: 37.83 %
Accuracy for 10 task(s): 	 [Class-IL]: 5.48 % 	 [Task-IL]: 33.59 %

>   Average： 7.002 / 38.87

### buffer size = 500

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 100

args.lr = 0.05   
args.minibatch_size = 32
args.batch_size = 32
args.hal_lambda = 1 
args.beta = 0.5
args.gamma = 5   
args.buffer_size = 500
```

Accuracy for 1 task(s): 	 [Class-IL]: 84.9 % 	 [Task-IL]: 84.9 %
Accuracy for 2 task(s): 	 [Class-IL]: 37.3 % 	 [Task-IL]: 59.25 %
Accuracy for 3 task(s): 	 [Class-IL]: 25.0 % 	 [Task-IL]: 54.57 %
Accuracy for 4 task(s): 	 [Class-IL]: 20.32 % 	 [Task-IL]: 50.72 %
Accuracy for 5 task(s): 	 [Class-IL]: 13.36 % 	 [Task-IL]: 46.02 %
Accuracy for 6 task(s): 	 [Class-IL]: 12.03 % 	 [Task-IL]: 49.95 %
Accuracy for 7 task(s): 	 [Class-IL]: 10.79 % 	 [Task-IL]: 44.8 %
Accuracy for 8 task(s): 	 [Class-IL]: 11.05 % 	 [Task-IL]: 48.7 %
Accuracy for 9 task(s): 	 [Class-IL]: 7.6 % 	 [Task-IL]: 47.29 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.78 % 	 [Task-IL]: 48.03 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.55 % 	 [Task-IL]: 43.37 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.29 % 	 [Task-IL]: 38.86 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.14 % 	 [Task-IL]: 41.48 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.41 % 	 [Task-IL]: 47.81 %

>   Average： 6.576 / 43.91

### buffer size = 5120

```
#    args.dataset = 'seq-cifar100'
#    args.print_freq = 5
#    args.n_epochs = 100
#
#    args.lr = 1e-1   #ok
#    args.minibatch_size = 32
#    args.batch_size = 32 #ok
#    
#    args.hal_lambda = 0 # ok 
#    args.beta = 0.5 #ok
#    args.gamma = 1   
#    args.buffer_size = 5120
```

Accuracy for 1 task(s): 	 [Class-IL]: 76.2 % 	 [Task-IL]: 76.2 %
Accuracy for 2 task(s): 	 [Class-IL]: 53.2 % 	 [Task-IL]: 65.75 %
Accuracy for 3 task(s): 	 [Class-IL]: 34.57 % 	 [Task-IL]: 57.6 %
Accuracy for 4 task(s): 	 [Class-IL]: 39.48 % 	 [Task-IL]: 62.48 %
Accuracy for 5 task(s): 	 [Class-IL]: 27.54 % 	 [Task-IL]: 57.48 %
Accuracy for 6 task(s): 	 [Class-IL]: 26.17 % 	 [Task-IL]: 57.58 %
Accuracy for 7 task(s): 	 [Class-IL]: 25.09 % 	 [Task-IL]: 61.23 %
Accuracy for 8 task(s): 	 [Class-IL]: 22.1 % 	 [Task-IL]: 58.19 %
Accuracy for 9 task(s): 	 [Class-IL]: 19.11 % 	 [Task-IL]: 55.16 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.52 % 	 [Task-IL]: 53.08 %
Accuracy for 10 task(s): 	 [Class-IL]: 17.25 % 	 [Task-IL]: 51.93 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.63 % 	 [Task-IL]: 53.77 %
Accuracy for 10 task(s): 	 [Class-IL]: 17.28 % 	 [Task-IL]: 52.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 16.19 % 	 [Task-IL]: 54.61 %

>   Average： 16.374 / 53.256
