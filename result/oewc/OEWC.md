# OEWC

> Title: Progress & Compress: A scalable framework for continual learning
>
> Conference: ICML2018
>
> Url: [Proceedings of ICML 2018(proceedings.mlr.press/)](https://proceedings.mlr.press/v80/schwarz18a.html)

```
@InProceedings{pmlr-v80-schwarz18a,
  title = 	 {Progress &amp; Compress: A scalable framework for continual learning},
  author =       {Schwarz, Jonathan and Czarnecki, Wojciech and Luketina, Jelena and Grabska-Barwinska, Agnieszka and Teh, Yee Whye and Pascanu, Razvan and Hadsell, Raia},
  pages = 	 {4528--4537},
  year = 	 {2018},
  volume = 	 {80},
  series = 	 {Proceedings of Machine Learning Research},
  month = 	 {10--15 Jul},
  publisher =    {PMLR},
}
```

### CIFAR10

```
args.dataset = 'seq-cifar10'
args.print_freq = 10
args.n_epochs = 50  

args.lr = 0.03
args.batch_size = 32
args.gamma = 1.0
args.e_lambda = 10
```

Accuracy for 1 task(s): 	 [Class-IL]: 97.4 % 	 [Task-IL]: 97.4 %
Accuracy for 2 task(s): 	 [Class-IL]: 45.78 % 	 [Task-IL]: 59.2 %
Accuracy for 3 task(s): 	 [Class-IL]: 31.28 % 	 [Task-IL]: 55.82 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.59 % 	 [Task-IL]: 63.21 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.45 % 	 [Task-IL]: 70.28 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.34 % 	 [Task-IL]: 56.14 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.42 % 	 [Task-IL]: 67.69 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.12 % 	 [Task-IL]: 62.07 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.41 % 	 [Task-IL]: 64.68 %

>   Average：19.348 / 64.172

#### MNIST

```
# args.dataset = 'seq-mnist'
# args.print_freq = 1
# args.n_epochs = 10
#
# args.lr = 0.001
# args.batch_size = 8
# args.gamma = 1.0
# args.e_lambda = 120
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.86 % 	 [Task-IL]: 99.86 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.3 % 	 [Task-IL]: 98.85 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.89 % 	 [Task-IL]: 98.29 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.9 % 	 [Task-IL]: 98.89 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.45 % 	 [Task-IL]: 98.04 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.56 % 	 [Task-IL]: 98.58 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.35 % 	 [Task-IL]: 97.88 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.75 % 	 [Task-IL]: 98.3 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.91 % 	 [Task-IL]: 98.33 %

>   Average：19.604 / 98.226

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100  # 不变

args.lr = 0.2
args.batch_size = 16
args.gamma = 1.0
args.e_lambda = 25
```

Accuracy for 1 task(s): 	 [Class-IL]: 71.7 % 	 [Task-IL]: 71.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 31.1 % 	 [Task-IL]: 52.95 %
Accuracy for 3 task(s): 	 [Class-IL]: 23.27 % 	 [Task-IL]: 45.4 %
Accuracy for 4 task(s): 	 [Class-IL]: 19.73 % 	 [Task-IL]: 42.55 %
Accuracy for 5 task(s): 	 [Class-IL]: 15.7 % 	 [Task-IL]: 38.94 %
Accuracy for 6 task(s): 	 [Class-IL]: 12.92 % 	 [Task-IL]: 39.5 %
Accuracy for 7 task(s): 	 [Class-IL]: 10.51 % 	 [Task-IL]: 33.14 %
Accuracy for 8 task(s): 	 [Class-IL]: 9.22 % 	 [Task-IL]: 33.51 %
Accuracy for 9 task(s): 	 [Class-IL]: 7.57 % 	 [Task-IL]: 32.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.42 % 	 [Task-IL]: 33.06 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.3 % 	 [Task-IL]: 28.92 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.67 % 	 [Task-IL]: 31.54 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.57 % 	 [Task-IL]: 32.92 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.14 % 	 [Task-IL]: 27.8 %

>   Average: 7.42/30.848

#### CIFAR-100

```
args.dataset = 'seq-cifar100'
args.print_freq = 10
args.n_epochs = 100 

args.lr = 0.02
args.batch_size = 32
args.gamma = 2
args.e_lambda = 5
```

Accuracy for 1 task(s): 	 [Class-IL]: 80.8 % 	 [Task-IL]: 80.8 %
Accuracy for 2 task(s): 	 [Class-IL]: 41.35 % 	 [Task-IL]: 57.55 %
Accuracy for 3 task(s): 	 [Class-IL]: 28.23 % 	 [Task-IL]: 50.03 %
Accuracy for 4 task(s): 	 [Class-IL]: 21.08 % 	 [Task-IL]: 39.88 %
Accuracy for 5 task(s): 	 [Class-IL]: 16.9 % 	 [Task-IL]: 34.64 %
Accuracy for 6 task(s): 	 [Class-IL]: 13.58 % 	 [Task-IL]: 33.87 %
Accuracy for 7 task(s): 	 [Class-IL]: 12.67 % 	 [Task-IL]: 33.47 %
Accuracy for 8 task(s): 	 [Class-IL]: 10.25 % 	 [Task-IL]: 32.6 %
Accuracy for 9 task(s): 	 [Class-IL]: 9.29 % 	 [Task-IL]: 31.83 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.18 % 	 [Task-IL]: 40.98 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.49 % 	 [Task-IL]: 39.81 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.03 % 	 [Task-IL]: 35.63 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.72 % 	 [Task-IL]: 38.79 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.87 % 	 [Task-IL]: 36.77 %

> Average: 8.658 / 38.396
