# GSS

> Title: Gradient based sample selection for online continual learning
>
> Conference: Neurips2019
>
> Url: [Advances in Neural Information Processing Systems 32 (NeurIPS 2019)](https://proceedings.neurips.cc/paper/2019/hash/e562cd9c0768d5464b64cf61da7fc6bb-Abstract.html)

```
@inproceedings{NEURIPS2019_e562cd9c,
 author = {Aljundi, Rahaf and Lin, Min and Goujaud, Baptiste and Bengio, Yoshua},
 booktitle = {Advances in Neural Information Processing Systems},
 editor = {H. Wallach and H. Larochelle and A. Beygelzimer and F. d\textquotesingle Alch\'{e}-Buc and E. Fox and R. Garnett},
 pages = {},
 publisher = {Curran Associates, Inc.},
 title = {Gradient based sample selection for online continual learning},
 url = {https://proceedings.neurips.cc/paper/2019/file/e562cd9c0768d5464b64cf61da7fc6bb-Paper.pdf},
 volume = {32},
 year = {2019}
}
```

#### CIFAR10

### buffer_size = 200

```
args.dataset = 'seq-cifar10'
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.minibatch_size = 32
args.gss_minibatch_size = 32
args.batch_size = 32
args.batch_num = 1
args.buffer_size = 200
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.7 % 	 [Task-IL]: 98.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 69.27 % 	 [Task-IL]: 89.4 %
Accuracy for 3 task(s): 	 [Class-IL]: 45.15 % 	 [Task-IL]: 89.45 %
Accuracy for 4 task(s): 	 [Class-IL]: 34.89 % 	 [Task-IL]: 86.56 %
Accuracy for 5 task(s): 	 [Class-IL]: 30.04 % 	 [Task-IL]: 85.5 %
Accuracy for 5 task(s): 	 [Class-IL]: 29.81 % 	 [Task-IL]: 88.64 %
Accuracy for 5 task(s): 	 [Class-IL]: 28.04 % 	 [Task-IL]: 87.43 %
Accuracy for 5 task(s): 	 [Class-IL]: 27.56 % 	 [Task-IL]: 87.2 %
Accuracy for 5 task(s): 	 [Class-IL]: 28.63 % 	 [Task-IL]: 87.85 %

>   Average：28.816 / 87.324

### buffer_size = 500

```
args.dataset = 'seq-cifar10'
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.minibatch_size = 32
args.gss_minibatch_size = 32
args.batch_size = 32
args.batch_num = 1
args.buffer_size = 500
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.55 % 	 [Task-IL]: 98.55 %
Accuracy for 2 task(s): 	 [Class-IL]: 71.32 % 	 [Task-IL]: 93.12 %
Accuracy for 3 task(s): 	 [Class-IL]: 53.25 % 	 [Task-IL]: 91.65 %
Accuracy for 4 task(s): 	 [Class-IL]: 41.84 % 	 [Task-IL]: 89.4 %
Accuracy for 5 task(s): 	 [Class-IL]: 38.54 % 	 [Task-IL]: 89.48 %
Accuracy for 5 task(s): 	 [Class-IL]: 39.52 % 	 [Task-IL]: 89.31 %
Accuracy for 5 task(s): 	 [Class-IL]: 33.67 % 	 [Task-IL]: 90.36 %
Accuracy for 5 task(s): 	 [Class-IL]: 34.83 % 	 [Task-IL]: 91.16 %
Accuracy for 5 task(s): 	 [Class-IL]: 35.67 % 	 [Task-IL]: 89.67 %

>   Average：36.446 / 89.996

### buffer_size = 5120

```
args.dataset = 'seq-cifar10'
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.minibatch_size = 32
args.gss_minibatch_size = 32
args.batch_size = 32
args.batch_num = 1
args.buffer_size = 5120
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.05 % 	 [Task-IL]: 99.05 %
Accuracy for 2 task(s): 	 [Class-IL]: 88.5 % 	 [Task-IL]: 93.15 %
Accuracy for 3 task(s): 	 [Class-IL]: 64.53 % 	 [Task-IL]: 93.7 %
Accuracy for 4 task(s): 	 [Class-IL]: 52.88 % 	 [Task-IL]: 93.01 %
Accuracy for 5 task(s): 	 [Class-IL]: 42.97 % 	 [Task-IL]: 90.82 %
Accuracy for 5 task(s): 	 [Class-IL]: 48.51 % 	 [Task-IL]: 92.6 %
Accuracy for 5 task(s): 	 [Class-IL]: 58.51 % 	 [Task-IL]: 94.01 %
Accuracy for 5 task(s): 	 [Class-IL]: 45.79 % 	 [Task-IL]: 90.95 %
Accuracy for 5 task(s): 	 [Class-IL]: 55.27 % 	 [Task-IL]: 94.32 %

>   Average：50.21 / 92.54

#### MNIST

### buffer_size = 200

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 10
args.gss_minibatch_size = 10
args.batch_size = 128
args.batch_num = 1
args.buffer_size = 200
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 96.03 % 	 [Task-IL]: 99.45 %
Accuracy for 3 task(s): 	 [Class-IL]: 67.29 % 	 [Task-IL]: 97.88 %
Accuracy for 4 task(s): 	 [Class-IL]: 48.87 % 	 [Task-IL]: 94.01 %
Accuracy for 5 task(s): 	 [Class-IL]: 39.09 % 	 [Task-IL]: 97.67 %
Accuracy for 5 task(s): 	 [Class-IL]: 40.87 % 	 [Task-IL]: 97.22 %
Accuracy for 5 task(s): 	 [Class-IL]: 38.69 % 	 [Task-IL]: 96.96 %
Accuracy for 5 task(s): 	 [Class-IL]: 37.76 % 	 [Task-IL]: 96.54 %
Accuracy for 5 task(s): 	 [Class-IL]: 38.76 % 	 [Task-IL]: 96.29 %

>   Average：39.034 / 96.936

### buffer_size = 500

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 10
args.gss_minibatch_size = 10
args.batch_size = 128
args.batch_num = 1
args.buffer_size = 500
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 96.1 % 	 [Task-IL]: 99.66 %
Accuracy for 3 task(s): 	 [Class-IL]: 85.19 % 	 [Task-IL]: 99.39 %
Accuracy for 4 task(s): 	 [Class-IL]: 79.19 % 	 [Task-IL]: 99.29 %
Accuracy for 5 task(s): 	 [Class-IL]: 60.94 % 	 [Task-IL]: 98.55 %
Accuracy for 5 task(s): 	 [Class-IL]: 62.1 % 	 [Task-IL]: 98.46 %
Accuracy for 5 task(s): 	 [Class-IL]: 62.26 % 	 [Task-IL]: 97.99 %
Accuracy for 5 task(s): 	 [Class-IL]: 63.01 % 	 [Task-IL]: 98.68 %
Accuracy for 5 task(s): 	 [Class-IL]: 64.17 % 	 [Task-IL]: 99.1 %

>   Average：62.496 / 98.556

### buffer_size = 5120

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.minibatch_size = 10
args.gss_minibatch_size = 10
args.batch_size = 128
args.batch_num = 1
args.buffer_size = 5120
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 97.57 % 	 [Task-IL]: 99.57 %
Accuracy for 3 task(s): 	 [Class-IL]: 92.95 % 	 [Task-IL]: 99.7 %
Accuracy for 4 task(s): 	 [Class-IL]: 92.03 % 	 [Task-IL]: 99.5 %
Accuracy for 5 task(s): 	 [Class-IL]: 87.53 % 	 [Task-IL]: 99.6 %
Accuracy for 5 task(s): 	 [Class-IL]: 79.81 % 	 [Task-IL]: 99.46 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.82 % 	 [Task-IL]: 99.48 %
Accuracy for 5 task(s): 	 [Class-IL]: 86.28 % 	 [Task-IL]: 99.41 %
Accuracy for 5 task(s): 	 [Class-IL]: 85.92 % 	 [Task-IL]: 99.16 %

>   Average：85.072 / 99.422

#### CIFAR100

### buffer_size = 200

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50  # ??

args.lr = 0.03
args.minibatch_size = 32
args.gss_minibatch_size = 32
args.batch_size = 32
args.batch_num = 1
args.buffer_size = 200
```

Accuracy for 1 task(s): 	 [Class-IL]: 80.3 % 	 [Task-IL]: 80.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.75 % 	 [Task-IL]: 79.8 %
Accuracy for 3 task(s): 	 [Class-IL]: 33.4 % 	 [Task-IL]: 71.2 %
Accuracy for 4 task(s): 	 [Class-IL]: 25.37 % 	 [Task-IL]: 67.42 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.54 % 	 [Task-IL]: 61.86 %
Accuracy for 6 task(s): 	 [Class-IL]: 16.43 % 	 [Task-IL]: 54.18 %
Accuracy for 7 task(s): 	 [Class-IL]: 14.43 % 	 [Task-IL]: 48.29 %
Accuracy for 8 task(s): 	 [Class-IL]: 12.35 % 	 [Task-IL]: 47.51 %
Accuracy for 9 task(s): 	 [Class-IL]: 10.52 % 	 [Task-IL]: 43.9 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.16 % 	 [Task-IL]: 47.63 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.12 % 	 [Task-IL]: 49.84 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.36 % 	 [Task-IL]: 49.52 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.97 % 	 [Task-IL]: 41.69 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.62 % 	 [Task-IL]: 49.7 %

>   Average：10.246 / 47.676

### buffer_size = 500

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.gss_minibatch_size = 32
args.batch_size = 32
args.batch_num = 1
args.buffer_size = 500
```

Accuracy for 1 task(s): 	 [Class-IL]: 86.5 % 	 [Task-IL]: 86.5 %
Accuracy for 2 task(s): 	 [Class-IL]: 55.65 % 	 [Task-IL]: 79.85 %
Accuracy for 3 task(s): 	 [Class-IL]: 37.1 % 	 [Task-IL]: 73.03 %
Accuracy for 4 task(s): 	 [Class-IL]: 27.3 % 	 [Task-IL]: 67.42 %
Accuracy for 5 task(s): 	 [Class-IL]: 22.02 % 	 [Task-IL]: 64.14 %
Accuracy for 6 task(s): 	 [Class-IL]: 18.15 % 	 [Task-IL]: 61.02 %
Accuracy for 7 task(s): 	 [Class-IL]: 16.26 % 	 [Task-IL]: 54.87 %
Accuracy for 8 task(s): 	 [Class-IL]: 13.3 % 	 [Task-IL]: 53.68 %
Accuracy for 9 task(s): 	 [Class-IL]: 11.92 % 	 [Task-IL]: 48.92 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.4 % 	 [Task-IL]: 52.03 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.92 % 	 [Task-IL]: 54.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.19 % 	 [Task-IL]: 55.69 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.28 % 	 [Task-IL]: 55.3 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.11 % 	 [Task-IL]: 51.51 %

>   Average：11.18 / 53.854

### buffer_size = 5120

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.03
args.minibatch_size = 32
args.gss_minibatch_size = 32
args.batch_size = 32
args.batch_num = 1
args.buffer_size = 5120
```

Accuracy for 1 task(s): 	 [Class-IL]: 85.6 % 	 [Task-IL]: 85.6 %
Accuracy for 2 task(s): 	 [Class-IL]: 72.2 % 	 [Task-IL]: 84.75 %
Accuracy for 3 task(s): 	 [Class-IL]: 48.93 % 	 [Task-IL]: 78.27 %
Accuracy for 4 task(s): 	 [Class-IL]: 37.4 % 	 [Task-IL]: 74.22 %
Accuracy for 5 task(s): 	 [Class-IL]: 29.54 % 	 [Task-IL]: 69.92 %
Accuracy for 6 task(s): 	 [Class-IL]: 24.73 % 	 [Task-IL]: 67.32 %
Accuracy for 7 task(s): 	 [Class-IL]: 21.29 % 	 [Task-IL]: 63.37 %
Accuracy for 8 task(s): 	 [Class-IL]: 18.35 % 	 [Task-IL]: 60.04 %
Accuracy for 9 task(s): 	 [Class-IL]: 16.61 % 	 [Task-IL]: 55.5 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.11 % 	 [Task-IL]: 60.22 %
Accuracy for 10 task(s): 	 [Class-IL]: 14.84 % 	 [Task-IL]: 58.41 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.36 % 	 [Task-IL]: 60.47 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.38 % 	 [Task-IL]: 60.56 %
Accuracy for 10 task(s): 	 [Class-IL]: 15.33 % 	 [Task-IL]: 62.97 %

>   Average：15.204 / 60.526
