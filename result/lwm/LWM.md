# LWM

> Title: Learning Without Memorizing
>
> Conference: CVPR2019
>
> Url: [CVPR 2019 open access(thecvf.com)](https://openaccess.thecvf.com/content_CVPR_2019/html/Dhar_Learning_Without_Memorizing_CVPR_2019_paper.html)

```
@InProceedings{Dhar_2019_CVPR,
author = {Dhar, Prithviraj and Singh, Rajat Vikram and Peng, Kuan-Chuan and Wu, Ziyan and Chellappa, Rama},
title = {Learning Without Memorizing},
booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)},
month = {June},
year = {2019}
}
```

#### MNIST

```
args.dataset = 'seq-mnist'
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.batch_size = 128
args.beta = 0.1
args.gamma = 0.3
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.79 % 	 [Task-IL]: 99.64 %
Accuracy for 3 task(s): 	 [Class-IL]: 31.26 % 	 [Task-IL]: 99.27 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.73 % 	 [Task-IL]: 81.79 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.69 % 	 [Task-IL]: 79.53 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.7 % 	 [Task-IL]: 80.84 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.2 % 	 [Task-IL]: 84.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.71 % 	 [Task-IL]: 84.06 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.64 % 	 [Task-IL]: 89.2 %

>   Average：19.588 / 89.2

#### CIFAR10


```
args.dataset = 'seq-cifar10'
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.batch_size = 32
args.beta = 1
args.gamma = 1
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.25 % 	 [Task-IL]: 98.25 %
Accuracy for 2 task(s): 	 [Class-IL]: 45.68 % 	 [Task-IL]: 92.78 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.95 % 	 [Task-IL]: 83.67 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.79 % 	 [Task-IL]: 85.14 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.6 % 	 [Task-IL]: 78.43 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.57 % 	 [Task-IL]: 78.92 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.46 % 	 [Task-IL]: 76.83 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.6 % 	 [Task-IL]: 77.16 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.66 % 	 [Task-IL]: 78.73 %

>   Average: 19.578 / 78.014

#### CIFAR100

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.5
args.batch_size = 16
args.beta = 5
args.gamma = 2
```

Accuracy for 1 task(s): 	 [Class-IL]: 79.6 % 	 [Task-IL]: 79.6 %
Accuracy for 2 task(s): 	 [Class-IL]: 38.95 % 	 [Task-IL]: 78.4 %
Accuracy for 3 task(s): 	 [Class-IL]: 27.3 % 	 [Task-IL]: 77.07 %
Accuracy for 4 task(s): 	 [Class-IL]: 20.82 % 	 [Task-IL]: 75.75 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.28 % 	 [Task-IL]: 76.76 %
Accuracy for 6 task(s): 	 [Class-IL]: 15.35 % 	 [Task-IL]: 74.0 %
Accuracy for 7 task(s): 	 [Class-IL]: 13.33 % 	 [Task-IL]: 71.61 %
Accuracy for 8 task(s): 	 [Class-IL]: 10.76 % 	 [Task-IL]: 68.97 %
Accuracy for 9 task(s): 	 [Class-IL]: 10.19 % 	 [Task-IL]: 67.14 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.45 % 	 [Task-IL]: 69.14 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.54 % 	 [Task-IL]: 69.79 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.07 % 	 [Task-IL]: 67.36 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.52 % 	 [Task-IL]: 69.49 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.64 % 	 [Task-IL]: 68.63 %

>   Average: 9.44 / 68.88

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.5
args.batch_size = 16
args.beta = 1
args.gamma = 5
```

Accuracy for 1 task(s): 	 [Class-IL]: 69.0 % 	 [Task-IL]: 69.0 %
Accuracy for 2 task(s): 	 [Class-IL]: 32.1 % 	 [Task-IL]: 65.35 %
Accuracy for 3 task(s): 	 [Class-IL]: 24.5 % 	 [Task-IL]: 65.67 %
Accuracy for 4 task(s): 	 [Class-IL]: 19.98 % 	 [Task-IL]: 63.7 %
Accuracy for 5 task(s): 	 [Class-IL]: 16.04 % 	 [Task-IL]: 62.68 %
Accuracy for 6 task(s): 	 [Class-IL]: 12.65 % 	 [Task-IL]: 59.03 %
Accuracy for 7 task(s): 	 [Class-IL]: 11.13 % 	 [Task-IL]: 54.81 %
Accuracy for 8 task(s): 	 [Class-IL]: 9.7 % 	 [Task-IL]: 50.06 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.08 % 	 [Task-IL]: 48.76 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.8 % 	 [Task-IL]: 47.22 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.18 % 	 [Task-IL]: 49.65 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.12 % 	 [Task-IL]: 49.42 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.85 % 	 [Task-IL]: 47.3 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.83 % 	 [Task-IL]: 48.19 %

>   Average: 7.956 / 48.356

#### Tiny-ImageNet-25split

    args.dataset = 'seq-tinyimg'
    args.print_freq = 5
    args.n_epochs = 50
    args.nt = 25
    
    args.lr = 0.5
    args.batch_size = 16
    args.beta = 1
    args.gamma = 5

Accuracy for 1 task(s): 	 [Class-IL]: 65.75 % 	 [Task-IL]: 65.75 %
Accuracy for 2 task(s): 	 [Class-IL]: 41.12 % 	 [Task-IL]: 74.75 %
Accuracy for 3 task(s): 	 [Class-IL]: 27.5 % 	 [Task-IL]: 75.5 %
Accuracy for 4 task(s): 	 [Class-IL]: 20.12 % 	 [Task-IL]: 72.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 15.55 % 	 [Task-IL]: 71.6 %
Accuracy for 6 task(s): 	 [Class-IL]: 12.58 % 	 [Task-IL]: 65.67 %
Accuracy for 7 task(s): 	 [Class-IL]: 12.46 % 	 [Task-IL]: 65.11 %
Accuracy for 8 task(s): 	 [Class-IL]: 10.56 % 	 [Task-IL]: 63.66 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.78 % 	 [Task-IL]: 61.03 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.18 % 	 [Task-IL]: 61.95 %
Accuracy for 11 task(s): 	 [Class-IL]: 7.75 % 	 [Task-IL]: 59.3 %
Accuracy for 12 task(s): 	 [Class-IL]: 7.23 % 	 [Task-IL]: 59.15 %
Accuracy for 13 task(s): 	 [Class-IL]: 6.56 % 	 [Task-IL]: 58.54 %
Accuracy for 14 task(s): 	 [Class-IL]: 5.86 % 	 [Task-IL]: 55.11 %
Accuracy for 15 task(s): 	 [Class-IL]: 5.47 % 	 [Task-IL]: 52.67 %
Accuracy for 16 task(s): 	 [Class-IL]: 5.25 % 	 [Task-IL]: 51.55 %
Accuracy for 17 task(s): 	 [Class-IL]: 4.88 % 	 [Task-IL]: 50.41 %
Accuracy for 18 task(s): 	 [Class-IL]: 4.53 % 	 [Task-IL]: 50.46 %
Accuracy for 19 task(s): 	 [Class-IL]: 4.46 % 	 [Task-IL]: 48.29 %
Accuracy for 20 task(s): 	 [Class-IL]: 4.24 % 	 [Task-IL]: 48.94 %
Accuracy for 21 task(s): 	 [Class-IL]: 3.8 % 	 [Task-IL]: 48.07 %
Accuracy for 22 task(s): 	 [Class-IL]: 3.66 % 	 [Task-IL]: 46.22 %
Accuracy for 23 task(s): 	 [Class-IL]: 3.92 % 	 [Task-IL]: 48.37 %
Accuracy for 24 task(s): 	 [Class-IL]: 3.55 % 	 [Task-IL]: 45.55 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.25 % 	 [Task-IL]: 45.07 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.27 % 	 [Task-IL]: 47.74 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.31 % 	 [Task-IL]: 44.15 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.27 % 	 [Task-IL]: 45.92 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.32 % 	 [Task-IL]: 44.41 %

>   Average: 3.284 / 45.458

