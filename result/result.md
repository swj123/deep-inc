# Comparison Results

<table> 
    <tr>
        <th rowspan="2">Method</th>
        <th rowspan="2">Venues</th>
        <th rowspan="2">Memory Size</th>
        <th colspan="2">MNIST</th>
        <th colspan="2">CIFAR10</th>
        <th colspan="2">CIFAR100</th>
        <th colspan="2">Tiny-ImageNet</th>
    </tr >
        <th>Class-IL</th>
        <th>Task-IL</th>
        <th>Class-IL</th>
        <th>Task-IL</th>
        <th>Class-IL</th>
        <th>Task-IL</th>
        <th>Class-IL</th>
        <th>Task-IL</th>
    </tr >
    <tr>
	    <td>JOINT</td>
	    <td>-</td>
	    <td>-</td>
	    <td>97.454</td>
        <td>99.72</td>
        <td>91.286</td>
        <td>98.07</td>
	    <td>70.312</td>
	    <td>91.182</td>
        <td>58.072</td>
        <td>82.006</td>
	</tr>
	<tr>
	    <td>PNN</td>
	    <td>arxiv2016</td>
	    <td>-</td>
	    <td>-</td>
        <td>99.752</td>
        <td>-</td>
        <td>95.228</td>
	    <td>-</td>
	    <td>67.948</td>
        <td>-</td>
        <td>63.744</td>
	</tr>
    <tr>
	    <td>LWF</td>
	    <td>PAMI2017</td>
	    <td>-</td>
	    <td>21.622</td>
        <td>99.154</td>
        <td>19.586</td>
        <td>91.906</td>
	    <td>10.048</td>
	    <td>63.78</td>
        <td>9.362</td>
        <td>58.606</td>
	</tr>
	<tr>
	    <td>SI</td>
	    <td>ICML2017</td>
	    <td>-</td>
	    <td>20.854</td>
        <td>99.36</td>
        <td>19.466</td>
        <td>69.404</td>
	    <td>7.42</td>
	    <td>62.21</td>
        <td>6.774</td>
        <td>59.932</td>
	</tr>
	<tr>
	    <td>DGR</td>
	    <td>NIPS2017</td>
	    <td>-</td>
        <td>21.39</td>
        <td>91.056</td>
	    <td>19.556</td>
        <td>70.51</td>
	    <td>9.334 </td>
	    <td>44.534</td>
        <td>-</td>
        <td>-</td>
	</tr>
 	<tr>
	    <td rowspan="3">GEM</td>
	    <td rowspan="3">NIPS2017</td>
	    <td>200</td>
	    <td>71.434</td>
        <td>98.452</td>
        <td>24.216</td>
        <td>86.598</td>
	    <td>10.828</td>
	    <td>60.23</td>
        <td>-</td>
        <td>-</td>
	</tr>
    <tr>
	    <td>500</td>
	    <td>85.64</td>
        <td>97.74</td>
        <td>23.658</td>
        <td>85.138</td>
	    <td>11.756</td>
	    <td>62.796</td>
        <td>-</td>
        <td>-</td>
	</tr>
    <tr>
	    <td>5120</td>
	    <td>85.90</td>
        <td>97.64</td>
        <td>24.382</td>
        <td>85.982</td>
	    <td>10.926</td>
	    <td>59.206</td>
        <td>-</td>
        <td>-</td>
	</tr>
	<tr>
	    <td>OEWC</td>
	    <td>ICML2018</td>
	    <td>-</td>
	    <td>19.604</td>
        <td>98.226</td>
        <td>19.348</td>
        <td>64.172</td>
	    <td>8.658</td>
	    <td>38.396</td>
        <td>7.42</td>
        <td>30.848</td>
	</tr>
	<tr>
	    <td>LWM</td>
	    <td>CVPR2019</td>
	    <td>-</td>
	    <td>19.588</td>
        <td>89.2</td>
        <td>19.578</td>
        <td>78.014</td>
	    <td>9.44</td>
	    <td>68.88</td>
        <td>7.956</td>
        <td>48.356</td>
	</tr>
    <tr>
	    <td rowspan="3">GSS</td>
	    <td rowspan="3">NIPS2019</td>
	    <td>200</td>
	    <td>39.034 </td>
        <td>96.936</td>
        <td>28.816</td>
        <td>87.324</td>
	    <td>10.246</td>
	    <td>47.676</td>
        <td>-</td>
        <td>-</td>
	</tr>
    <tr>
	    <td>500</td>
	    <td>62.496 </td>
        <td>98.556</td>
        <td>36.446 </td>
        <td>89.996</td>
	    <td>11.18</td>
	    <td>53.854</td>
        <td>-</td>
        <td>-</td>
	</tr>
    <tr>
	    <td>5120</td>
	    <td>85.072</td>
        <td>99.422</td>
        <td>50.21 </td>
        <td>92.54</td>
	    <td>15.204</td>
	    <td>60.526</td>
        <td>-</td>
        <td>-</td>
	</tr>
	<tr>
	    <td>Deep-Inversion</td>
	    <td>CVPR2020</td>
	    <td>-</td>
	    <td>20.692</td>
        <td>95.424</td>
        <td>24.142</td>
        <td>94.458</td>
	    <td>11.192</td>
	    <td>68.43</td>
        <td>8.19</td>
        <td>66.118</td>
	</tr>
	<tr>
	    <td rowspan="3">DER</td>
	    <td rowspan="3">NIPS2020</td>
	    <td>200</td>
	    <td>83.438 </td>
        <td>98.828</td>
        <td>44.294</td>
        <td>87.084</td>
	    <td>11.306 </td>
	    <td>58.24</td>
        <td>9.13 </td>
        <td>36.064</td>
	</tr>
    <tr>
	    <td>500</td>
	    <td>90.356 </td>
        <td>99.016</td>
        <td>54.562</td>
        <td>91.404</td>
	    <td>17.812 </td>
	    <td>68.716</td>
        <td>10.758 </td>
        <td>46.232</td>
	</tr>
    <tr>
	    <td>5120</td>
	    <td>95.446</td>
        <td>99.39</td>
        <td>74.154</td>
        <td>94.894</td>
	    <td>44.864</td>
	    <td>83.484</td>
        <td>33.048 </td>
        <td>66.848</td>
	</tr>
	<tr>
	    <td>PASS</td>
	    <td>CVPR2021</td>
	    <td>-</td>
	    <td>76.85</td>
        <td>94.926</td>
        <td>53.118</td>
        <td>86.074</td>
	    <td>31.802</td>
	    <td>77.308</td>
        <td>28.478</td>
        <td>62.874</td>
	</tr>
	<tr>
	    <td>GPM</td>
	    <td>ICLR2021</td>
	    <td>-</td>
	    <td>19.62</td>
        <td>93.3</td>
        <td>18.884</td>
        <td>85.996</td>
	    <td>8.188 </td>
	    <td>74.944</td>
        <td>7.04</td>
        <td>63.41</td>
	</tr>
    <tr>
	    <td rowspan="3">HAL</td>
	    <td rowspan="3">AAAI2021</td>
	    <td>200</td>
	    <td>79.056 </td>
        <td>98.914</td>
        <td>19.73</td>
        <td>75.356</td>
	    <td>7.002</td>
	    <td>38.87</td>
        <td>-</td>
        <td>-</td>
	</tr>
    <tr>
	    <td>500</td>
	    <td>87.364 </td>
        <td>99.114</td>
        <td>23.952 </td>
        <td>78.01</td>
	    <td>6.576</td>
	    <td>43.91</td>
        <td>-</td>
        <td>-</td>
	</tr>
    <tr>
	    <td>5120</td>
	    <td>94.914</td>
        <td>99.512</td>
        <td>44.532</td>
        <td>82.34</td>
	    <td>16.374</td>
	    <td>53.256</td>
        <td>-</td>
        <td>-</td>
	</tr>
	<tr>
	    <td>DisCOIL</td>
	    <td>arxiv2022</td>
	    <td>-</td>
        <td>96.69</td>
        <td>99.61</td>
        <td>44.54</td>
        <td>90.428</td>
	    <td>25.798</td>
	    <td>72.2</td>
        <td>19.746</td>
	    <td>53.08</td>
	</tr>
	<tr>
	    <td rowspan="3">CLS-ER</td>
	    <td rowspan="3">ICLR2022</td>
	    <td>200</td>
	    <td>83.34</td>
        <td>98.84</td>
	    <td>48.59</td>
        <td>91.94</td>
	    <td>15.28</td>
        <td>61.31</td>
	    <td> </td>
        <td></td>
	</tr>
    <tr>
	    <td>500</td>
	    <td>82.21</td>
        <td>98.53</td>
        <td>64.11</td>
        <td>93.53</td>
	    <td>24.72 </td>
	    <td>72.11</td>
        <td>14.91 </td>
        <td>57.24</td>
	</tr>
    <tr>
	    <td>5120</td>
	    <td>83.88</td>
        <td>98.85</td>
	    <td>84.96</td>
        <td>96.93</td>
	    <td>55.83</td>
        <td>87.36</td>
	    <td> </td>
        <td></td>
	</tr>
</table>
