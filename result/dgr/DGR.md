# DGR

> Title: Continual Learning with Deep Generative Replay
>
> Conference: NIPS2017
>
> URL: https://proceedings.neurips.cc/paper/2017/hash/0efbe98067c6c73dba1250d2beaa81f9-Abstract.html

```
@inproceedings{NIPS2017_0efbe980,
 author = {Shin, Hanul and Lee, Jung Kwon and Kim, Jaehong and Kim, Jiwon},
 booktitle = {Advances in Neural Information Processing Systems},
 title = {Continual Learning with Deep Generative Replay},
 volume = {30},
 year = {2017}
}
```

### CIFAR100

```
args.dataset = 'seq-cifar100'
args.n_epochs = 100
args.print_freq = 10

args.lr = 0.05
args.batch_size = 64
args.mu = 1
```

Accuracy for 1 task(s): 	 [Class-IL]: 87.2 % 	 [Task-IL]: 87.2 %
Accuracy for 2 task(s): 	 [Class-IL]: 45.65 % 	 [Task-IL]: 76.6 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.37 % 	 [Task-IL]: 71.77 %
Accuracy for 4 task(s): 	 [Class-IL]: 22.9 % 	 [Task-IL]: 61.62 %
Accuracy for 5 task(s): 	 [Class-IL]: 17.82 % 	 [Task-IL]: 59.3 %
Accuracy for 6 task(s): 	 [Class-IL]: 15.1 % 	 [Task-IL]: 54.4 %
Accuracy for 7 task(s): 	 [Class-IL]: 13.26 % 	 [Task-IL]: 47.76 %
Accuracy for 8 task(s): 	 [Class-IL]: 11.06 % 	 [Task-IL]: 44.45 %
Accuracy for 9 task(s): 	 [Class-IL]: 10.04 % 	 [Task-IL]: 42.62 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.42 % 	 [Task-IL]: 47.45 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.29 % 	 [Task-IL]: 46.43 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.33 % 	 [Task-IL]: 39.94 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.37 % 	 [Task-IL]: 44.25 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.26 % 	 [Task-IL]: 44.6 %

>   Average: 9.334 / 44.534

### CIFAR10

```
args.dataset = 'seq-cifar10'
args.n_epochs = 50
args.print_freq = 10

args.lr = 0.1
args.batch_size = 128
args.mu = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 95.35 % 	 [Task-IL]: 95.35 %
Accuracy for 2 task(s): 	 [Class-IL]: 44.1 % 	 [Task-IL]: 89.8 %
Accuracy for 3 task(s): 	 [Class-IL]: 32.02 % 	 [Task-IL]: 88.07 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.7 % 	 [Task-IL]: 81.64 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.59 % 	 [Task-IL]: 78.2 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.63 % 	 [Task-IL]: 80.48 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.63 % 	 [Task-IL]: 61.12 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.37 % 	 [Task-IL]: 66.5 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.56 % 	 [Task-IL]: 66.25 %

>   Average: 19.556/70.51

### MNIST

```
args.dataset = 'seq-mnist'
args.print_freq = 10
args.n_epochs = 10

args.lr = 1e-3
args.batch_size = 50
args.mu = 1
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.81 % 	 [Task-IL]: 99.81 %
Accuracy for 2 task(s): 	 [Class-IL]: 66.37 % 	 [Task-IL]: 97.52 %
Accuracy for 3 task(s): 	 [Class-IL]: 39.43 % 	 [Task-IL]: 91.78 %
Accuracy for 4 task(s): 	 [Class-IL]: 29.95 % 	 [Task-IL]: 94.61 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.79 % 	 [Task-IL]: 89.42 %
Accuracy for 5 task(s): 	 [Class-IL]: 22.21 % 	 [Task-IL]: 89.31 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.94 % 	 [Task-IL]: 92.83 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.79 % 	 [Task-IL]: 92.02 %
Accuracy for 5 task(s): 	 [Class-IL]: 23.22 % 	 [Task-IL]: 91.7 %

>   Average: 21.39 / 91.056

