# JOINT

> Title: joint training
>
> Journal: -
>
> Url: -


### CIFAR10

```
# args.dataset = 'seq-cifar10'
# args.print_freq = 10
# args.n_epochs = 50 
#
# args.lr = 0.03
# args.batch_size = 32
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 50.0 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 49.85 %
Accuracy for 3 task(s): 	 [Class-IL]: 16.67 % 	 [Task-IL]: 49.9 %
Accuracy for 4 task(s): 	 [Class-IL]: 12.5 % 	 [Task-IL]: 49.92 %
Accuracy for 5 task(s): 	 [Class-IL]: 90.94 % 	 [Task-IL]: 98.1 %
Accuracy for 5 task(s): 	 [Class-IL]: 91.47 % 	 [Task-IL]: 97.98 %
Accuracy for 5 task(s): 	 [Class-IL]: 91.95 % 	 [Task-IL]: 98.26 %
Accuracy for 5 task(s): 	 [Class-IL]: 91.57 % 	 [Task-IL]: 98.18 %
Accuracy for 5 task(s): 	 [Class-IL]: 90.5 % 	 [Task-IL]: 97.83 %

>   Average：91.286 / 98.07

#### MNIST

```
args.dataset = 'seq-mnist'
args.nt = 5
args.isPretrain = False
args.print_freq = 1
args.n_epochs = 10

# args.lr = 0.03
# args.batch_size = 32         
```

Accuracy for 1 task(s): 	 [Class-IL]: 40.47 % 	 [Task-IL]: 47.04 %
Accuracy for 2 task(s): 	 [Class-IL]: 21.17 % 	 [Task-IL]: 43.23 %
Accuracy for 3 task(s): 	 [Class-IL]: 16.27 % 	 [Task-IL]: 46.87 %
Accuracy for 4 task(s): 	 [Class-IL]: 13.48 % 	 [Task-IL]: 54.33 %
Accuracy for 5 task(s): 	 [Class-IL]: 97.51 % 	 [Task-IL]: 99.75 %
Accuracy for 5 task(s): 	 [Class-IL]: 97.31 % 	 [Task-IL]: 99.66 %
Accuracy for 5 task(s): 	 [Class-IL]: 97.42 % 	 [Task-IL]: 99.78 %
Accuracy for 5 task(s): 	 [Class-IL]: 97.46 % 	 [Task-IL]: 99.69 %
Accuracy for 5 task(s): 	 [Class-IL]: 97.57 % 	 [Task-IL]: 99.72 %

>   Average：97.454 / 99.72

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.isPretrain = False
args.print_freq = 10
args.n_epochs = 100 

# args.lr = 0.03
# args.batch_size = 32   
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 5.0 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 5.0 %
Accuracy for 3 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 5.0 %
Accuracy for 4 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 5.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 5.0 %
Accuracy for 6 task(s): 	 [Class-IL]: 0.83 % 	 [Task-IL]: 5.0 %
Accuracy for 7 task(s): 	 [Class-IL]: 0.71 % 	 [Task-IL]: 5.0 %
Accuracy for 8 task(s): 	 [Class-IL]: 0.62 % 	 [Task-IL]: 4.91 %
Accuracy for 9 task(s): 	 [Class-IL]: 0.56 % 	 [Task-IL]: 4.93 %
Accuracy for 10 task(s): 	 [Class-IL]: 58.52 % 	 [Task-IL]: 81.86 %
Accuracy for 10 task(s): 	 [Class-IL]: 58.1 % 	 [Task-IL]: 82.0 %
Accuracy for 10 task(s): 	 [Class-IL]: 58.57 % 	 [Task-IL]: 82.07 %
Accuracy for 10 task(s): 	 [Class-IL]: 57.44 % 	 [Task-IL]: 82.09 %
Accuracy for 10 task(s): 	 [Class-IL]: 57.73 % 	 [Task-IL]: 82.01 %

>   Average：58.072 / 82.006

### CIFAR100

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.01
args.batch_size = 64
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 10.0 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 10.0 %
Accuracy for 3 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 9.73 %
Accuracy for 4 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 9.78 %
Accuracy for 5 task(s): 	 [Class-IL]: 1.92 % 	 [Task-IL]: 9.82 %
Accuracy for 6 task(s): 	 [Class-IL]: 1.6 % 	 [Task-IL]: 9.82 %
Accuracy for 7 task(s): 	 [Class-IL]: 1.37 % 	 [Task-IL]: 9.84 %
Accuracy for 8 task(s): 	 [Class-IL]: 1.2 % 	 [Task-IL]: 9.86 %
Accuracy for 9 task(s): 	 [Class-IL]: 1.07 % 	 [Task-IL]: 9.88 %
Accuracy for 10 task(s): 	 [Class-IL]: 70.83 % 	 [Task-IL]: 91.22 %
Accuracy for 10 task(s): 	 [Class-IL]: 70.42 % 	 [Task-IL]: 91.27 %
Accuracy for 10 task(s): 	 [Class-IL]: 69.46 % 	 [Task-IL]: 91.09 %
Accuracy for 10 task(s): 	 [Class-IL]: 70.92 % 	 [Task-IL]: 91.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 69.93 % 	 [Task-IL]: 90.59 %

>   Average：70.312 / 91.182
