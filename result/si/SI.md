# SI

> Title: Continual learning through synaptic intelligence
>
> Conference: ICML2017
>
> Url: [Continual Learning Through Synaptic Intelligence (mlr.press)](http://proceedings.mlr.press/v70/zenke17a/zenke17a.pdf)

```
@inproceedings{si,
  title={Continual learning through synaptic intelligence},
  author={Zenke, Friedemann and Poole, Ben and Ganguli, Surya},
  booktitle=ICML,
  pages={3987--3995},
  year={2017}
}
```

### MNIST

```
# args.dataset = 'seq-mnist'
# args.nt = 5
# args.print_freq = 10
# args.n_epochs = 10
#
# args.lr = 0.05
# args.batch_size = 8
# args.c = 2.0
# args.xi = 1.0
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 49.15 % 	 [Task-IL]: 99.59 %
Accuracy for 3 task(s): 	 [Class-IL]: 32.57 % 	 [Task-IL]: 99.29 %
Accuracy for 4 task(s): 	 [Class-IL]: 26.13 % 	 [Task-IL]: 99.54 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.99 % 	 [Task-IL]: 99.3 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.66 % 	 [Task-IL]: 99.32 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.22 % 	 [Task-IL]: 99.32 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.45 % 	 [Task-IL]: 99.45 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.95 % 	 [Task-IL]: 99.41 %

>   Average：20.854 / 99.36

### CIFAR10

```
#args.dataset = 'seq-cifar10'
#args.nt = 5
#args.print_freq = 10
#args.n_epochs = 50 

#args.lr = 0.03
#args.batch_size = 32
#args.c = 0.5
#args.xi = 1.0
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.7 % 	 [Task-IL]: 98.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 40.28 % 	 [Task-IL]: 66.7 %
Accuracy for 3 task(s): 	 [Class-IL]: 31.9 % 	 [Task-IL]: 74.28 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.7 % 	 [Task-IL]: 68.35 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.53 % 	 [Task-IL]: 66.23 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.08 % 	 [Task-IL]: 69.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.51 % 	 [Task-IL]: 69.06 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.6 % 	 [Task-IL]: 71.66 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.61 % 	 [Task-IL]: 71.07 %

>   Average: 19.466/69.404

### CIFAR100

```
# args.dataset = 'seq-cifar100'
# args.print_freq = 10
# args.n_epochs = 100

# args.lr = 0.1
# args.batch_size = 16
# args.c = 0.2
# args.xi = 1.0
```

Accuracy for 1 task(s): 	 [Class-IL]: 83.6 % 	 [Task-IL]: 83.6 %
Accuracy for 2 task(s): 	 [Class-IL]: 40.9 % 	 [Task-IL]: 73.15 %
Accuracy for 3 task(s): 	 [Class-IL]: 26.83 % 	 [Task-IL]: 68.57 %
Accuracy for 4 task(s): 	 [Class-IL]: 17.62 % 	 [Task-IL]: 60.88 %
Accuracy for 5 task(s): 	 [Class-IL]: 15.88 % 	 [Task-IL]: 65.26 %
Accuracy for 6 task(s): 	 [Class-IL]: 13.3 % 	 [Task-IL]: 64.97 %
Accuracy for 7 task(s): 	 [Class-IL]: 12.54 % 	 [Task-IL]: 60.63 %
Accuracy for 8 task(s): 	 [Class-IL]: 9.56 % 	 [Task-IL]: 61.66 %
Accuracy for 9 task(s): 	 [Class-IL]: 9.32 % 	 [Task-IL]: 65.04 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.97 % 	 [Task-IL]: 63.48 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.11 % 	 [Task-IL]: 60.29 %
Accuracy for 10 task(s): 	 [Class-IL]: 7.93 % 	 [Task-IL]: 59.18 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.6 % 	 [Task-IL]: 65.89 %

>   Average： 7.42 / 62.21

### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100  # 不变
#
args.lr = 0.05 # [0.03, 0.05, 0.01, 0.003, 0.005]
args.batch_size = 16
args.c = 0.5
args.xi = 1.0

```

Accuracy for 1 task(s): 	 [Class-IL]: 78.7 % 	 [Task-IL]: 78.7 %
Accuracy for 2 task(s): 	 [Class-IL]: 31.5 % 	 [Task-IL]: 57.0 %
Accuracy for 3 task(s): 	 [Class-IL]: 21.67 % 	 [Task-IL]: 59.97 %
Accuracy for 4 task(s): 	 [Class-IL]: 17.18 % 	 [Task-IL]: 59.2 %
Accuracy for 5 task(s): 	 [Class-IL]: 14.38 % 	 [Task-IL]: 64.32 %
Accuracy for 6 task(s): 	 [Class-IL]: 11.87 % 	 [Task-IL]: 65.6 %
Accuracy for 7 task(s): 	 [Class-IL]: 9.34 % 	 [Task-IL]: 63.8 %
Accuracy for 8 task(s): 	 [Class-IL]: 8.45 % 	 [Task-IL]: 64.11 %
Accuracy for 9 task(s): 	 [Class-IL]: 6.97 % 	 [Task-IL]: 62.62 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.85 % 	 [Task-IL]: 61.06 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.83 % 	 [Task-IL]: 59.38 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.88 % 	 [Task-IL]: 60.34 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.61 % 	 [Task-IL]: 57.37 %
Accuracy for 10 task(s): 	 [Class-IL]: 6.7 % 	 [Task-IL]: 61.51 %

>   Average： 6.774 / 59.932

### Tiny-ImageNet-25split

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100  # 不变
args.nt = 25

args.lr = 0.05  # [0.03, 0.05, 0.01, 0.003, 0.005]
args.batch_size = 16
args.c = 0.5
args.xi = 1.0
```

Accuracy for 1 task(s): 	 [Class-IL]: 80.25 % 	 [Task-IL]: 80.25 %
Accuracy for 2 task(s): 	 [Class-IL]: 41.5 % 	 [Task-IL]: 67.25 %
Accuracy for 3 task(s): 	 [Class-IL]: 24.25 % 	 [Task-IL]: 58.92 %
Accuracy for 4 task(s): 	 [Class-IL]: 18.62 % 	 [Task-IL]: 57.0 %
Accuracy for 5 task(s): 	 [Class-IL]: 14.75 % 	 [Task-IL]: 51.25 %
Accuracy for 6 task(s): 	 [Class-IL]: 10.79 % 	 [Task-IL]: 48.42 %
Accuracy for 7 task(s): 	 [Class-IL]: 11.61 % 	 [Task-IL]: 49.18 %
Accuracy for 8 task(s): 	 [Class-IL]: 10.34 % 	 [Task-IL]: 54.94 %
Accuracy for 9 task(s): 	 [Class-IL]: 8.89 % 	 [Task-IL]: 56.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.33 % 	 [Task-IL]: 59.05 %
Accuracy for 11 task(s): 	 [Class-IL]: 8.25 % 	 [Task-IL]: 60.02 %
Accuracy for 12 task(s): 	 [Class-IL]: 6.6 % 	 [Task-IL]: 59.71 %
Accuracy for 13 task(s): 	 [Class-IL]: 6.21 % 	 [Task-IL]: 51.75 %
Accuracy for 14 task(s): 	 [Class-IL]: 5.21 % 	 [Task-IL]: 52.21 %
Accuracy for 15 task(s): 	 [Class-IL]: 4.4 % 	 [Task-IL]: 49.83 %
Accuracy for 16 task(s): 	 [Class-IL]: 4.22 % 	 [Task-IL]: 48.83 %
Accuracy for 17 task(s): 	 [Class-IL]: 3.87 % 	 [Task-IL]: 45.65 %
Accuracy for 18 task(s): 	 [Class-IL]: 3.68 % 	 [Task-IL]: 47.6 %
Accuracy for 19 task(s): 	 [Class-IL]: 3.54 % 	 [Task-IL]: 50.46 %
Accuracy for 20 task(s): 	 [Class-IL]: 3.04 % 	 [Task-IL]: 44.89 %
Accuracy for 21 task(s): 	 [Class-IL]: 2.74 % 	 [Task-IL]: 47.5 %
Accuracy for 22 task(s): 	 [Class-IL]: 2.6 % 	 [Task-IL]: 42.16 %
Accuracy for 23 task(s): 	 [Class-IL]: 2.83 % 	 [Task-IL]: 46.88 %
Accuracy for 24 task(s): 	 [Class-IL]: 2.95 % 	 [Task-IL]: 49.01 %
Accuracy for 25 task(s): 	 [Class-IL]: 1.92 % 	 [Task-IL]: 35.03 %
Accuracy for 25 task(s): 	 [Class-IL]: 2.11 % 	 [Task-IL]: 37.95 %
Accuracy for 25 task(s): 	 [Class-IL]: 2.2 % 	 [Task-IL]: 40.58 %
Accuracy for 25 task(s): 	 [Class-IL]: 2.05 % 	 [Task-IL]: 36.48 %
Accuracy for 25 task(s): 	 [Class-IL]: 2.61 % 	 [Task-IL]: 49.15 %

>   Average: 2.176 / 39.838











