# LWF

> Title: Learning without forgetting
>
> Journal: PAMI2017
>
> Url: [Learning without Forgetting | IEEE Journals & Magazine | IEEE Xplore](https://ieeexplore.ieee.org/abstract/document/8107520)

```
@article{lwf,
  title={Learning without forgetting},
  author={Li, Zhizhong and Hoiem, Derek},
  journal=PAMI,
  volume={40},
  number={12},
  pages={2935--2947},
  year={2017}
}
```

#### MNIST

```
args.dataset = 'seq-mnist'
args.nt = 5
args.print_freq = 1
args.n_epochs = 10 

args.lr = 0.02
args.batch_size = 64
args.alpha = 5
args.softmax_temp = 2.0
args.wd_reg = 5e-5       
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.72 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 48.59 % 	 [Task-IL]: 99.45 %
Accuracy for 3 task(s): 	 [Class-IL]: 30.96 % 	 [Task-IL]: 99.47 %
Accuracy for 4 task(s): 	 [Class-IL]: 28.12 % 	 [Task-IL]: 99.38 %
Accuracy for 5 task(s): 	 [Class-IL]: 22.33 % 	 [Task-IL]: 99.18 %
Accuracy for 5 task(s): 	 [Class-IL]: 20.33 % 	 [Task-IL]: 99.16 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.81 % 	 [Task-IL]: 99.17 %
Accuracy for 5 task(s): 	 [Class-IL]: 21.01 % 	 [Task-IL]: 99.06 %
Accuracy for 5 task(s): 	 [Class-IL]: 22.63 % 	 [Task-IL]: 99.2 %

>   Average: $21.622\pm0.85  / 99.154\pm0.05$

### CIFAR10

```
# args.dataset = 'seq-cifar10'
# args.print_freq = 10
# args.n_epochs = 50 
#
# args.lr = 0.01
# args.batch_size = 32
# args.alpha = 3.0
# args.softmax_temp = 2.0
# args.wd_reg = 0.0005
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.6 % 	 [Task-IL]: 98.6 %
Accuracy for 2 task(s): 	 [Class-IL]: 46.2 % 	 [Task-IL]: 94.98 %
Accuracy for 3 task(s): 	 [Class-IL]: 31.53 % 	 [Task-IL]: 93.47 %
Accuracy for 4 task(s): 	 [Class-IL]: 24.65 % 	 [Task-IL]: 93.18 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.56 % 	 [Task-IL]: 91.3 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.6 % 	 [Task-IL]: 91.44 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.58 % 	 [Task-IL]: 92.25 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.77 % 	 [Task-IL]: 93.17 %
Accuracy for 5 task(s): 	 [Class-IL]: 19.42 % 	 [Task-IL]: 91.37 %

>   Average: $19.586\pm0.11 / 91.906\pm0.72$

#### CIFAR100

```
args.dataset = 'seq-cifar100'
args.print_freq = 10
args.n_epochs = 50

args.lr = 0.01
args.batch_size = 32
args.alpha = 3.0
args.softmax_temp = 2.0
args.wd_reg = 0.0005
```

Accuracy for 1 task(s): 	 [Class-IL]: 77.5 % 	 [Task-IL]: 77.5 %
Accuracy for 2 task(s): 	 [Class-IL]: 40.45 % 	 [Task-IL]: 78.2 %
Accuracy for 3 task(s): 	 [Class-IL]: 28.23 % 	 [Task-IL]: 75.0 %
Accuracy for 4 task(s): 	 [Class-IL]: 22.65 % 	 [Task-IL]: 74.18 %
Accuracy for 5 task(s): 	 [Class-IL]: 15.34 % 	 [Task-IL]: 60.96 %
Accuracy for 6 task(s): 	 [Class-IL]: 14.02 % 	 [Task-IL]: 60.33 %
Accuracy for 7 task(s): 	 [Class-IL]: 14.31 % 	 [Task-IL]: 61.51 %
Accuracy for 8 task(s): 	 [Class-IL]: 11.7 % 	 [Task-IL]: 61.38 %
Accuracy for 9 task(s): 	 [Class-IL]: 11.52 % 	 [Task-IL]: 63.43 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.14 % 	 [Task-IL]: 63.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.61 % 	 [Task-IL]: 68.04 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.0 % 	 [Task-IL]: 61.92 %
Accuracy for 10 task(s): 	 [Class-IL]: 8.88 % 	 [Task-IL]: 56.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 10.61 % 	 [Task-IL]: 68.46 %

>   Average：10.048 / 63.78

#### Tiny-ImageNet

```
# args.dataset = 'seq-tinyimg'
# args.print_freq = 10
# args.n_epochs = 100  # 不变
#
# args.lr = 0.01
# args.batch_size = 32
# args.alpha = 1.0
# args.softmax_temp = 2.0
# args.wd_reg = 0.0005
```

Accuracy for 1 task(s): 	 [Class-IL]: 76.9 % 	 [Task-IL]: 76.9 %
Accuracy for 2 task(s): 	 [Class-IL]: 38.5 % 	 [Task-IL]: 73.15 %
Accuracy for 3 task(s): 	 [Class-IL]: 28.77 % 	 [Task-IL]: 72.33 %
Accuracy for 4 task(s): 	 [Class-IL]: 25.1 % 	 [Task-IL]: 71.23 %
Accuracy for 5 task(s): 	 [Class-IL]: 18.86 % 	 [Task-IL]: 68.84 %
Accuracy for 6 task(s): 	 [Class-IL]: 15.7 % 	 [Task-IL]: 66.17 %
Accuracy for 7 task(s): 	 [Class-IL]: 13.87 % 	 [Task-IL]: 64.0 %
Accuracy for 8 task(s): 	 [Class-IL]: 12.16 % 	 [Task-IL]: 62.3 %
Accuracy for 9 task(s): 	 [Class-IL]: 10.06 % 	 [Task-IL]: 60.17 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.49 % 	 [Task-IL]: 59.74 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.25 % 	 [Task-IL]: 55.13 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.46 % 	 [Task-IL]: 59.48 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.39 % 	 [Task-IL]: 58.98 %
Accuracy for 10 task(s): 	 [Class-IL]: 9.22 % 	 [Task-IL]: 59.7 %

>   Average：9.362 / 58.606

#### Tiny-ImageNet-25split

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100  # 不变
args.nt = 25

args.lr = 0.01
args.batch_size = 32
args.alpha = 1.0
args.softmax_temp = 2.0
args.wd_reg = 0.0005
```

Accuracy for 1 task(s): 	 [Class-IL]: 77.25 % 	 [Task-IL]: 77.25 %
Accuracy for 2 task(s): 	 [Class-IL]: 44.38 % 	 [Task-IL]: 80.62 %
Accuracy for 3 task(s): 	 [Class-IL]: 28.5 % 	 [Task-IL]: 78.5 %
Accuracy for 4 task(s): 	 [Class-IL]: 22.19 % 	 [Task-IL]: 75.19 %
Accuracy for 5 task(s): 	 [Class-IL]: 17.55 % 	 [Task-IL]: 73.35 %
Accuracy for 6 task(s): 	 [Class-IL]: 14.21 % 	 [Task-IL]: 69.58 %
Accuracy for 7 task(s): 	 [Class-IL]: 14.36 % 	 [Task-IL]: 67.82 %
Accuracy for 8 task(s): 	 [Class-IL]: 12.97 % 	 [Task-IL]: 67.81 %
Accuracy for 9 task(s): 	 [Class-IL]: 11.25 % 	 [Task-IL]: 66.11 %
Accuracy for 10 task(s): 	 [Class-IL]: 11.6 % 	 [Task-IL]: 66.07 %
Accuracy for 11 task(s): 	 [Class-IL]: 10.75 % 	 [Task-IL]: 63.52 %
Accuracy for 12 task(s): 	 [Class-IL]: 8.75 % 	 [Task-IL]: 62.4 %
Accuracy for 13 task(s): 	 [Class-IL]: 8.38 % 	 [Task-IL]: 59.88 %
Accuracy for 14 task(s): 	 [Class-IL]: 7.54 % 	 [Task-IL]: 60.11 %
Accuracy for 15 task(s): 	 [Class-IL]: 7.12 % 	 [Task-IL]: 59.15 %
Accuracy for 16 task(s): 	 [Class-IL]: 6.08 % 	 [Task-IL]: 55.64 %
Accuracy for 17 task(s): 	 [Class-IL]: 6.12 % 	 [Task-IL]: 52.75 %
Accuracy for 18 task(s): 	 [Class-IL]: 5.83 % 	 [Task-IL]: 50.69 %
Accuracy for 19 task(s): 	 [Class-IL]: 5.28 % 	 [Task-IL]: 49.58 %
Accuracy for 20 task(s): 	 [Class-IL]: 4.75 % 	 [Task-IL]: 48.14 %
Accuracy for 21 task(s): 	 [Class-IL]: 4.25 % 	 [Task-IL]: 49.02 %
Accuracy for 22 task(s): 	 [Class-IL]: 4.27 % 	 [Task-IL]: 46.14 %
Accuracy for 23 task(s): 	 [Class-IL]: 4.64 % 	 [Task-IL]: 46.62 %
Accuracy for 24 task(s): 	 [Class-IL]: 4.34 % 	 [Task-IL]: 46.43 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.93 % 	 [Task-IL]: 45.59 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.97 % 	 [Task-IL]: 44.68 %
Accuracy for 25 task(s): 	 [Class-IL]: 4.05 % 	 [Task-IL]: 46.18 %
Accuracy for 25 task(s): 	 [Class-IL]: 4.15 % 	 [Task-IL]: 43.58 %
Accuracy for 25 task(s): 	 [Class-IL]: 3.94 % 	 [Task-IL]: 47.46 %

>   Average: 4.008 / 45.498

