# PASS

> Title: Prototype augmentation and self-supervision for incremental learning
>
> Conference: CVPR2021
>
> Url: [CVPR 2021 Open Access Repository (thecvf.com)](https://openaccess.thecvf.com/content/CVPR2021/html/Zhu_Prototype_Augmentation_and_Self-Supervision_for_Incremental_Learning_CVPR_2021_paper.html)

```
@InProceedings{pass,
    author    = {Zhu, Fei and Zhang, Xu-Yao and Wang, Chuang and Yin, Fei and Liu, Cheng-Lin},
    title     = {Prototype augmentation and self-supervision for incremental learning},
    booktitle = CVPR,
    month     = {June},
    year      = {2021},
    pages     = {5871-5880}
}
```

### MNIST

```
args.dataset = 'seq-mnist'
args.nt = 5
args.isPretrain = False
args.print_freq = 10
args.n_epochs = 10 

args.lr = 1e-4               
args.batch_size = 128         
args.protoAug_weight = 0.5   
args.kd_weight = 10         
args.temp = 0.1             
```

Accuracy for 1 task(s): 	 [Class-IL]: 99.95 % 	 [Task-IL]: 99.95 %
Accuracy for 2 task(s): 	 [Class-IL]: 91.85 % 	 [Task-IL]: 95.98 %
Accuracy for 3 task(s): 	 [Class-IL]: 85.52 % 	 [Task-IL]: 94.69 %
Accuracy for 4 task(s): 	 [Class-IL]: 82.79 % 	 [Task-IL]: 95.4 %
Accuracy for 5 task(s): 	 [Class-IL]: 77.53 % 	 [Task-IL]: 95.1 %
Accuracy for 5 task(s): 	 [Class-IL]: 76.83 % 	 [Task-IL]: 94.59 %
Accuracy for 5 task(s): 	 [Class-IL]: 76.93 % 	 [Task-IL]: 95.58 %
Accuracy for 5 task(s): 	 [Class-IL]: 76.79 % 	 [Task-IL]: 94.24 %
Accuracy for 5 task(s): 	 [Class-IL]: 76.17 % 	 [Task-IL]: 95.12 %

>   Average：76.85 / 94.926

### CIFAR10

```
args.dataset = 'seq-cifar10'
args.nt = 5
args.isPretrain = False
args.print_freq = 10
args.n_epochs = 50  

args.lr = 1e-4               
args.batch_size = 64  
args.protoAug_weight = 0.2      
args.kd_weight = 10
args.temp = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 98.85 % 	 [Task-IL]: 98.85 %
Accuracy for 2 task(s): 	 [Class-IL]: 76.58 % 	 [Task-IL]: 89.12 %
Accuracy for 3 task(s): 	 [Class-IL]: 62.2 % 	 [Task-IL]: 87.17 %
Accuracy for 4 task(s): 	 [Class-IL]: 56.84 % 	 [Task-IL]: 87.31 %
Accuracy for 5 task(s): 	 [Class-IL]: 53.49 % 	 [Task-IL]: 86.06 %
Accuracy for 5 task(s): 	 [Class-IL]: 53.28 % 	 [Task-IL]: 85.73 %
Accuracy for 5 task(s): 	 [Class-IL]: 52.61 % 	 [Task-IL]: 86.27 %
Accuracy for 5 task(s): 	 [Class-IL]: 52.96 % 	 [Task-IL]: 86.11 %
Accuracy for 5 task(s): 	 [Class-IL]: 53.25 % 	 [Task-IL]: 86.2 %

>   Average：53.118 / 86.074

### CIFAR100

    args.dataset = 'seq-cifar100'
    args.isPretrain = False
    args.print_freq = 10
    args.n_epochs = 100
    
    args.lr = 5e-4
    args.batch_size = 64
    args.protoAug_weight = 0.05
    args.kd_weight = 0.2
    args.temp = 0.1

Accuracy for 1 task(s): 	 [Class-IL]: 91.3 % 	 [Task-IL]: 91.3 %
Accuracy for 2 task(s): 	 [Class-IL]: 71.35 % 	 [Task-IL]: 86.75 %
Accuracy for 3 task(s): 	 [Class-IL]: 62.03 % 	 [Task-IL]: 84.13 %
Accuracy for 4 task(s): 	 [Class-IL]: 55.18 % 	 [Task-IL]: 82.02 %
Accuracy for 5 task(s): 	 [Class-IL]: 48.4 % 	 [Task-IL]: 80.66 %
Accuracy for 6 task(s): 	 [Class-IL]: 44.98 % 	 [Task-IL]: 79.42 %
Accuracy for 7 task(s): 	 [Class-IL]: 42.23 % 	 [Task-IL]: 78.04 %
Accuracy for 8 task(s): 	 [Class-IL]: 37.6 % 	 [Task-IL]: 77.36 %
Accuracy for 9 task(s): 	 [Class-IL]: 35.52 % 	 [Task-IL]: 76.08 %
Accuracy for 10 task(s): 	 [Class-IL]: 31.57 % 	 [Task-IL]: 76.9 %
Accuracy for 10 task(s): 	 [Class-IL]: 32.69 % 	 [Task-IL]: 77.53 %
Accuracy for 10 task(s): 	 [Class-IL]: 32.41 % 	 [Task-IL]: 77.73 %
Accuracy for 10 task(s): 	 [Class-IL]: 31.58 % 	 [Task-IL]: 76.69 %
Accuracy for 10 task(s): 	 [Class-IL]: 30.76 % 	 [Task-IL]: 77.69 %

> Average: 31.802 / 77.308

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.isPretrain = False
args.print_freq = 10
args.n_epochs = 100 

args.lr = 1e-4                  
args.batch_size = 32 
args.protoAug_weight = 0.5     
args.kd_weight = 10           
args.temp = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 80.1 % 	 [Task-IL]: 80.1 %
Accuracy for 2 task(s): 	 [Class-IL]: 57.6 % 	 [Task-IL]: 70.7 %
Accuracy for 3 task(s): 	 [Class-IL]: 48.73 % 	 [Task-IL]: 67.47 %
Accuracy for 4 task(s): 	 [Class-IL]: 45.28 % 	 [Task-IL]: 67.15 %
Accuracy for 5 task(s): 	 [Class-IL]: 41.48 % 	 [Task-IL]: 66.36 %
Accuracy for 6 task(s): 	 [Class-IL]: 38.23 % 	 [Task-IL]: 65.03 %
Accuracy for 7 task(s): 	 [Class-IL]: 35.83 % 	 [Task-IL]: 64.64 %
Accuracy for 8 task(s): 	 [Class-IL]: 33.84 % 	 [Task-IL]: 63.96 %
Accuracy for 9 task(s): 	 [Class-IL]: 31.23 % 	 [Task-IL]: 63.19 %
Accuracy for 10 task(s): 	 [Class-IL]: 29.4 % 	 [Task-IL]: 63.44 %
Accuracy for 10 task(s): 	 [Class-IL]: 27.72 % 	 [Task-IL]: 62.4 %
Accuracy for 10 task(s): 	 [Class-IL]: 28.2 % 	 [Task-IL]: 62.58 %
Accuracy for 10 task(s): 	 [Class-IL]: 28.66 % 	 [Task-IL]: 62.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 28.41 % 	 [Task-IL]: 63.06 %

> Average: 28.478 / 62.874

#### Tiny-ImageNet-25split

```
args.dataset = 'seq-tinyimg'
args.isPretrain = False
args.print_freq = 10
args.n_epochs = 100 
args.nt = 25

args.lr = 1e-4                  
args.batch_size = 32 
args.protoAug_weight = 0.5     
args.kd_weight = 10           
args.temp = 0.1
```

Accuracy for 1 task(s): 	 [Class-IL]: 82.5 % 	 [Task-IL]: 82.5 %
Accuracy for 2 task(s): 	 [Class-IL]: 61.5 % 	 [Task-IL]: 76.62 %
Accuracy for 3 task(s): 	 [Class-IL]: 56.0 % 	 [Task-IL]: 74.5 %
Accuracy for 4 task(s): 	 [Class-IL]: 47.5 % 	 [Task-IL]: 72.06 %
Accuracy for 5 task(s): 	 [Class-IL]: 44.2 % 	 [Task-IL]: 72.35 %
Accuracy for 6 task(s): 	 [Class-IL]: 38.12 % 	 [Task-IL]: 69.67 %
Accuracy for 7 task(s): 	 [Class-IL]: 36.96 % 	 [Task-IL]: 70.36 %
Accuracy for 8 task(s): 	 [Class-IL]: 35.06 % 	 [Task-IL]: 69.88 %
Accuracy for 9 task(s): 	 [Class-IL]: 34.03 % 	 [Task-IL]: 69.92 %
Accuracy for 10 task(s): 	 [Class-IL]: 33.78 % 	 [Task-IL]: 70.2 %
Accuracy for 11 task(s): 	 [Class-IL]: 32.57 % 	 [Task-IL]: 70.98 %
Accuracy for 12 task(s): 	 [Class-IL]: 30.92 % 	 [Task-IL]: 70.81 %
Accuracy for 13 task(s): 	 [Class-IL]: 29.48 % 	 [Task-IL]: 70.58 %
Accuracy for 14 task(s): 	 [Class-IL]: 28.75 % 	 [Task-IL]: 71.43 %
Accuracy for 15 task(s): 	 [Class-IL]: 27.05 % 	 [Task-IL]: 70.32 %
Accuracy for 16 task(s): 	 [Class-IL]: 26.97 % 	 [Task-IL]: 70.95 %
Accuracy for 17 task(s): 	 [Class-IL]: 25.41 % 	 [Task-IL]: 70.41 %
Accuracy for 18 task(s): 	 [Class-IL]: 25.26 % 	 [Task-IL]: 70.49 %
Accuracy for 19 task(s): 	 [Class-IL]: 23.61 % 	 [Task-IL]: 69.89 %
Accuracy for 20 task(s): 	 [Class-IL]: 22.85 % 	 [Task-IL]: 69.38 %
Accuracy for 21 task(s): 	 [Class-IL]: 21.79 % 	 [Task-IL]: 69.37 %
Accuracy for 22 task(s): 	 [Class-IL]: 22.26 % 	 [Task-IL]: 68.45 %
Accuracy for 23 task(s): 	 [Class-IL]: 20.83 % 	 [Task-IL]: 68.82 %
Accuracy for 24 task(s): 	 [Class-IL]: 20.07 % 	 [Task-IL]: 68.65 %
Accuracy for 25 task(s): 	 [Class-IL]: 20.04 % 	 [Task-IL]: 68.88 %
Accuracy for 25 task(s): 	 [Class-IL]: 20.26 % 	 [Task-IL]: 69.31 %
Accuracy for 25 task(s): 	 [Class-IL]: 20.43 % 	 [Task-IL]: 69.41 %
Accuracy for 25 task(s): 	 [Class-IL]: 20.35 % 	 [Task-IL]: 69.79 %
Accuracy for 25 task(s): 	 [Class-IL]: 20.86 % 	 [Task-IL]: 68.64 %

> Average: 20.388 / 69.206
