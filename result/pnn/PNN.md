# PNN

> Title: Progressive Neural Networks
>
> -
>
> Url: [Progressive Neural Networks](https://arxiv.org/abs/1606.04671v3)

```
@misc{rusu2016progressive,
      title={Progressive Neural Networks}, 
      author={Andrei A. Rusu and Neil C. Rabinowitz and Guillaume Desjardins and Hubert Soyer and James Kirkpatrick and Koray Kavukcuoglu and Razvan Pascanu and Raia Hadsell},
      year={2016},
      eprint={1606.04671},
      archivePrefix={arXiv},
      primaryClass={cs.LG}
}
```

#### CIFAR10

```
args.dataset = 'seq-cifar10'
args.nt = 5
args.print_freq = 5
args.n_epochs = 50  

args.lr = 0.03
args.batch_size = 32
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 98.8 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 94.68 %
Accuracy for 3 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 93.93 %
Accuracy for 4 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 94.84 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 95.16 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 94.93 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 95.54 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 95.27 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 95.24 %

>   Average：- / 95.228

#### MNIST

```
args.dataset = 'seq-mnist'
args.nt = 5
args.print_freq = 1
args.n_epochs = 10  

args.lr = 0.1
args.batch_size = 10
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.91 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.78 %
Accuracy for 3 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.78 %
Accuracy for 4 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.8 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.77 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.76 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.78 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.8 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 99.65 %

>   Average：- / 99.752

#### Tiny-ImageNet

```
args.dataset = 'seq-tinyimg'
args.print_freq = 10
args.n_epochs = 100  

args.lr = 0.03
args.batch_size = 32
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.6 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 20.75 %
Accuracy for 3 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.6 %
Accuracy for 4 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.98 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.7 %
Accuracy for 6 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.47 %
Accuracy for 7 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.56 %
Accuracy for 8 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.46 %
Accuracy for 9 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.18 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 20.99 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.24 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.97 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 22.87 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 21.53 %

>   Average：- / 21.72

#### CIFAR100

```
args.dataset = 'seq-cifar100'
args.print_freq = 5
args.n_epochs = 50

args.lr = 0.05
args.batch_size = 32
```

Accuracy for 1 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 78.5 %
Accuracy for 2 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 69.8 %
Accuracy for 3 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 72.8 %
Accuracy for 4 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 73.42 %
Accuracy for 5 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 74.18 %
Accuracy for 6 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 74.68 %
Accuracy for 7 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 75.01 %
Accuracy for 8 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 74.86 %
Accuracy for 9 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 74.77 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 71.13 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 67.89 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 65.66 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 68.95 %
Accuracy for 10 task(s): 	 [Class-IL]: 0.0 % 	 [Task-IL]: 66.11 %

>   Average：- / 67.948
