# -*- coding: utf-8 -*-

"""Top-level package for visintIncremental."""

__author__ = """Wenju Sun"""
__email__ = 'SunWenJu@bjtu.edu.cn'
__version__ = '0.1.0'

import deepinc.models as models
import deepinc.datasets as datasets
import deepinc.backbone as backbone
import deepinc.utils as utils
